'use strict';

angular
.module('tunecakes', [
   'ngRoute',
   'ngStorage',
   'ngAnimate',
   'ngResource',
   'ui.bootstrap',
   'ui.calendar',
   'ui.bootstrap.datetimepicker',
   'ui.mask',
   'ui.router',
   'ui.validate',
   'angularSpinner',
   'angulartics',
   'angulartics.mixpanel',
   'tcArtist',
   'tcArtistDropdown',
   'tcAuth',
   'tcConfig',
   'tcCrystalBall',
   'tcDirectives',
   'tcEvent',
   'tcEventCalendar',
   'tcFacebook',
   'tcFeed',
   'tcImgur',
   'tcLocation',
   'tcLogin',
   'tcMain',
   'tcMessages',
   'tcNavbar',
   'tcNoembed',
   'tcNotifications',
   'tcPasswordReset',
   'tcPost',
   'tcRegister',
   'tcRelease',
   'tcResources',
   'tcSearch',
   'tcSettings',
   'tcSpotify',
   'tcTags',
   'tcTours',
   'tcTwitter',
   'tcUser',
   'tcValidation'
])
.config(function ($locationProvider, $httpProvider, uiMaskConfigProvider,
   $analyticsProvider, $provide) {

   $locationProvider.html5Mode(true);
   $httpProvider.interceptors.push('AuthInterceptor');

   // Create extra mask definitions
   uiMaskConfigProvider = uiMaskConfigProvider.$get();
   var maskDefs = uiMaskConfigProvider.maskDefinitions;

   for (var i = 1; i < 9; i++) {
      maskDefs[i] = new RegExp("[0-" + i + "]");
   }

   maskDefs['m'] = /[mM]/;
   maskDefs['p'] = /[aApP]/;

   //$urlRouterProvider.deferIntercept();
   $analyticsProvider.firstPageview(false);

})
.run(function ($rootScope, $state, AuthService, $log, MessageService, $transitions, TunecakesConfig) {
   if (TunecakesConfig.debug) routeDebug($rootScope);

   $rootScope.util = util;

   $rootScope.goToObjectPage = function (object) {
      $state.go("parent." + object.type.toLowerCase(), {
         id: object.alias || object.id
      });
   };

   $(window).scroll(function () {
      var scrollTop = $(window).scrollTop();
      var docHeight = $(document).height();
      var winHeight = $(window).height();
      var scrollPercent = (scrollTop) / (docHeight - winHeight);

      $rootScope.scrollPct = Math.round(scrollPercent * 100);
      $rootScope.$apply();
   });

   function goToFeedIfLoggedIn() {
      AuthService.resolve().then(function () {
         if (AuthService.isLoggedIn()) {
            $state.go("parent.feed.events");
         }
      });
   }

   function checkForAuthOnlyRoutes(trans) {
      console.log("STATE CHANGE START", trans.router.stateService.get('access'));
      console.log("TO PARAMS", trans.entering())
      let declarations = trans.entering();

      // wait until it has resolved otherwise checks won't make sense
      AuthService.resolve().then(function () {
         declarations.forEach(d => {
            if (d.access && d.access.requiredLogin &&
               !AuthService.isLoggedIn()) {
               $state.go("parent.login");
            }
         });
      });
   }

   $transitions.onStart({to: "parent.main.**"}, goToFeedIfLoggedIn);
   $transitions.onStart({to: "parent.login"}, goToFeedIfLoggedIn);
   $transitions.onStart({to: "parent.register"}, goToFeedIfLoggedIn);
   $transitions.onStart({to: "parent.**"}, checkForAuthOnlyRoutes);
   
   // FIXME: Need better handling... Create a specific error page for each error.status?
   $rootScope.$on('$stateChangeError',
      function (event, toState, toParams, fromState, fromParams, error) {
         event.preventDefault();
         MessageService.parseHttpErrorAfterRedirect(
            $state.go("parent.main"), error);
      });
});

function routeDebug($rootScope) {
   // Credits: Adam's answer in http://stackoverflow.com/a/20786262/69362
   // Paste this in browser's console
   // var $rootScope = angular.element(document.querySelectorAll("[ui-view]")[0]).injector().get('$rootScope');

   $rootScope.$on('$stateChangeStart', function (event, toState, toParams,
      fromState, fromParams) {
      console.log('$stateChangeStart to ' + toState.to +
         '- fired when the transition begins. toState,toParams : \n',
         toState, toParams);
   });

   $rootScope.$on('$stateChangeError', function (event, toState, toParams,
      fromState, fromParams) {
      console.log(
         '$stateChangeError - fired when an error occurs during transition.'
      );
      console.log(arguments);
   });

   $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams,
      fromState, fromParams) {
      console.log('$stateChangeSuccess to ' + toState.name +
         '- fired once the state transition is complete.');
   });

   $rootScope.$on('$viewContentLoaded', function (event) {
      console.log('$viewContentLoaded - fired after dom rendered', event);
   });

   $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState,
      fromParams) {
      console.log('$stateNotFound ' + unfoundState.to +
         '  - fired when a state cannot be found by its name.');
      console.log(unfoundState, fromState, fromParams);
   });
}

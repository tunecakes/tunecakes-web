angular
.module('tcSearch', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.search', {
      abstract: true,
      url: '/search?query&{artist_page:int}&{event_page:int}&{tags:string}search_type&lat&lon',
      params: {
         tags: {array: true}
      },
      views: {
         '': {
            template: '<ui-view />',
         }
      },
      reloadOnSearch: false,
      access: {
         requiredLogin: false
      }
   })
   .state('parent.search.results', {
      url: '/results',
      templateUrl: '/partials/search.html',
      controller: 'searchViewCtrl',
      reloadOnSearch: false,
      access: {
         requiredLogin: false
      }
   });
})
.factory('SearchService', function ($http, $q, TunecakesConfig) {
   var activeQueryBuilders = {};

   function QueryBuilderFactory(id) {
      if (!id) throw 'QueryBuilderFactory must be given id';

      if (activeQueryBuilders[id]) return activeQueryBuilders[id];

      console.log('Creating new QueryBuilder ' + id);

      var activeResults = {},
         resultsCallback,
         errorCallback,
         useSearchArea = false,
         searchArea = {},
         searchFilter = '',
         searchText = '',
         search = {},
         artistPage = 0,
         eventPage = 0,
         tags,
         latitude,
         longitude,
         radius,
         random = false,
         searchType = 'search';

      function sendQuery(type, page) {
         var url = TunecakesConfig.apiAddress + '/api/search/' + type;

         var params = {
            latitude: latitude,
            longitude: longitude,
            page: page,
            radius: radius,
            random: random,
            tags: tags,
            text: searchText
         };

         return $http.get(url, {params: params})
         .then(function (data) {
            return data.data.hits.hits;
         });
      }

      // Catch and handle errors here, don't error out
      search.artist = function artistQuery() {
         var response = {
            artists: []
         };

         return sendQuery('artist', artistPage)
         .then(function (hits) {
            response.type = 'artists';
            response.artists = hits;

            return response;
         });
      };

      search.event = function eventQuery() {
         var response = {
            events: []
         };

         return sendQuery('event', eventPage)
         .then(function (hits) {
            response.type = 'events';
            response.events = hits;

            return response;
         });
      };

      function executeQuery() {
         var list,
            results = {},
            searchPromises;

         if (!search[searchFilter]) {
            searchPromises = [
               search.artist(),
               search.event()
            ];
         } else {
            searchPromises = [search[searchFilter]()];
         }

         return $q.all(searchPromises)
         .then(function (data) {
            for (var i = 0; i < data.length; i++) {
               list = data[i];
               results[list.type] = list[list.type];
            }

            if(resultsCallback) resultsCallback(results);
            return results
         })
         .catch(err => {
            if(errorCallback) errorCallback(err);
            throw err;
         });
      }

      function onResults(cb) {
         resultsCallback = cb;
      }

      function onError(cb) {
         errorCallback = cb;
      }

      function setSearchText(text) {
         searchText = text;
      }

      function setFilter(filter) {
         searchFilter = filter;
      }

      function setSearchType(type) {
         searchType = type;
      }

      function setArtistPage(page) {
         artistPage = page;
      }

      function setEventPage(page) {
         eventPage = page;
      }

      function setRandom(isRandom) {
         random = isRandom;
      }

      function setSearchArea(lat, lon, distance) {
         latitude = lat;
         longitude = lon;
         radius = distance;
      }

      function setTags(new_tags) {
         tags = new_tags;
      }

      function removeSearchArea() {
         radius = undefined;
      }

      activeQueryBuilders[id] = {
         executeQuery: executeQuery,
         setFilter: setFilter,
         setSearchText: setSearchText,
         setSearchType: setSearchType,
         setTags: setTags,
         setArtistPage: setArtistPage,
         setEventPage: setEventPage,
         setRandom: setRandom,
         onResults: onResults,
         onError: onError,
         setSearchArea: setSearchArea,
         removeSearchArea: removeSearchArea
      };

      return activeQueryBuilders[id];
   }

   return QueryBuilderFactory;
})
.controller('searchViewCtrl', function ($scope, $state, $stateParams, SearchService) {
   var lat, lon, QueryBuilder = SearchService('main');

   function updateResults(results) {
      console.log('Search results found', results);
      $scope.results = results;
   }

   function searchError(err) {
      console.error('QueryBuilder error', err);
   }

   $scope.results = {
      artists: [],
      events: []
   };


   $scope.locationStub = {};

   $scope.tags = $stateParams.tags || [];

   $scope.updateTags = function() {
      //$stateParams.tags = $scope.tags;
      //$stateParams.artist_page = 0;
      //$stateParams.event_page = 0;

      $state.go('parent.search.results', {
         tags: $scope.tags,
         artist_page: 0,
         event_page: 0
      });

      QueryBuilder.setTags($scope.tags);
      QueryBuilder.setEventPage($stateParams.event_page);
      QueryBuilder.setArtistPage($stateParams.artist_page);

      QueryBuilder.executeQuery();
   }

   $scope.params = $stateParams;

   $scope.nextArtistPage = function () {
      //$stateParams.artist_page = ($stateParams.artist_page || 0) + 1;
      $state.go('parent.search.results', {
         artist_page: ($stateParams.artist_page || 0) + 1
      });

      QueryBuilder.setArtistPage($stateParams.artist_page);
      QueryBuilder.executeQuery();
   };

   $scope.prevArtistPage = function () {
      if (!$stateParams.artist_page || $stateParams.artist_page <= 0) {
         return;
      }

      //$stateParams.artist_page = ($stateParams.artist_page || 0) - 1;
      $state.go('parent.search.results', {
         artist_page: ($stateParams.artist_page || 0) - 1
      });

      QueryBuilder.setArtistPage($stateParams.artist_page);
      QueryBuilder.executeQuery();
   };

   $scope.nextEventPage = function () {
      //$stateParams.event_page = ($stateParams.event_page || 0) + 1;
      $state.go('parent.search.results', {
         event_page: ($stateParams.event_page || 0) + 1
      });

      QueryBuilder.setEventPage($stateParams.event_page);
      QueryBuilder.executeQuery();
   };

   $scope.prevEventPage = function () {
      if (!$stateParams.event_page || $stateParams.event_page <= 0) {
         return;
      }

      //$stateParams.event_page = ($stateParams.event_page || 0) - 1;
      $state.go('parent.search.results', {
         event_page: ($stateParams.event_page || 0) - 1
      });

      QueryBuilder.setEventPage($stateParams.event_page);
      QueryBuilder.executeQuery();
   };

   $scope.artistFilterClick = function () {
      //$stateParams.search_type = $stateParams.search_type === 'artist' ? '' : 'artist';

      $state.go('parent.search.results', {
         search_type: $stateParams.search_type === 'artist' ? '' : 'artist'
      });
      QueryBuilder.setFilter($stateParams.search_type);
      QueryBuilder.executeQuery();
   };

   $scope.eventFilterClick = function () {
      //$stateParams.search_type = $stateParams.search_type === 'event' ? '' : 'event';

      $state.go('parent.search.results', {
         search_type: $stateParams.search_type === 'event' ? '' : 'event'
      });
      QueryBuilder.setFilter($stateParams.search_type);
      QueryBuilder.executeQuery();
   };

   $scope.locationFilter = 'worldFilter';
   $scope.closeFilterClick = function () {
      $scope.locationFilter = 'closeFilter';
      if (!lat || !lon) return;
      QueryBuilder.setSearchArea(lat, lon, '15km');
      QueryBuilder.executeQuery();
   };
   $scope.mediumFilterClick = function () {
      $scope.locationFilter = 'mediumFilter';
      if (!lat || !lon) return;
      QueryBuilder.setSearchArea(lat, lon, '50km');
      QueryBuilder.executeQuery();
   };
   $scope.farFilterClick = function () {
      $scope.locationFilter = 'farFilter';
      if (!lat || !lon) return;
      QueryBuilder.setSearchArea(lat, lon, '100km');
      QueryBuilder.executeQuery();
   };
   $scope.worldFilterClick = function () {
      $scope.locationFilter = 'worldFilter';
      QueryBuilder.removeSearchArea();
      QueryBuilder.executeQuery();
   };
   $scope.onLocationUpdate = function (location) {
      lat = location.lat;
      lon = location.lon;

      $state.go('parent.search.results', {
         lat: lat,
         lon: lon
      });

      if($scope.locationFilter === 'worldFilter') {
         $scope.mediumFilterClick();
         return;
      }

      $scope[$scope.locationFilter + 'Click']();
   };

   QueryBuilder.onResults(updateResults);
   QueryBuilder.onError(searchError);

   if($stateParams.lat && $stateParams.lon) {
      lat = parseFloat($stateParams.lat);
      lon = parseFloat($stateParams.lon);
      $scope.locationStub.location = {lat, lon};
      $scope.locationFilter = 'mediumFilter';
      QueryBuilder.setSearchArea(lat, lon, '50km');
   }
   QueryBuilder.setSearchText($stateParams.query);
   QueryBuilder.setArtistPage($stateParams.artist_page || 0);
   QueryBuilder.setTags($scope.tags);
   QueryBuilder.setFilter($stateParams.search_type || '');
   QueryBuilder.setEventPage($stateParams.event_page || 0);
   QueryBuilder.executeQuery();
})
.controller('searchCtrl', function ($analytics, $scope, $state, ArtistAPI, GoogleGeo, SearchService) {
   var QueryBuilder = SearchService('main');
   this.quickSearchMenuVisible = false;
   this.quickArtists = []
   this.quickLocations = [];

   this.quickSearch = (searchText) => {
      if(!searchText.length) {
         this.quickSearchMenuVisible = false;
         return;
      }

      ArtistAPI.autocomplete(searchText)
      .then(artists => {
         if(!artists.length) throw new Error("No artist results");

         artists = artists.slice(0, 3);
         this.quickArtists.splice(0, this.quickArtists.length);
         artists.forEach(a => this.quickArtists.push(a));
         this.quickSearchMenuVisible = true;
      }).catch(err => {
         this.quickArtists.splice(0, this.quickArtists.length);
         if(!this.quickLocations.length) {
            this.quickSearchMenuVisible = false;
         }
         console.log("Failed to find artists by name", searchText, err);
      });

      GoogleGeo.searchByString(searchText, true)
      .then(results => {
         if(!results.length) throw new Error("No location results");

         results = results.slice(0, 3);
         this.quickLocations.splice(0, this.quickLocations.length);
         results.forEach(r => this.quickLocations.push(r));
         this.quickSearchMenuVisible = true;
      }).catch(err => {
         this.quickLocations.splice(0, this.quickLocations.length);
         if(!this.quickArtists.length) {
            this.quickSearchMenuVisible = false;
         }
         console.log("Failed to get google location from search text", err);
      });
   }

   this.searchByLocation = loca => {
      $state.go('parent.search.results', {
           lat: loca.geometry.location.lat(),
           lon: loca.geometry.location.lng()
      }, {reload: true});

      this.quickSearchMenuVisible = false;
      this.searchText = "";
   };

   this.search = function search(searchText) {
      this.quickSearchMenuVisible = false;
      this.searchText = searchText;

      $state.go('parent.search.results', {
         query: searchText,
         artist_page: 0,
         event_page: 0
      });

      QueryBuilder.setSearchText(searchText);
      QueryBuilder.setArtistPage(0);
      QueryBuilder.setEventPage(0);

      $analytics.eventTrack("Search", {
         query: searchText
      });

      if ($state.includes('parent.search')) QueryBuilder.executeQuery();
   };
})
.directive('tcSearchFilters', function ($stateParams) {
   return {
      restrict: 'E',
      templateUrl: '/partials/search_filters.html'
   }
})
.directive('tcSearchbar', function ($stateParams) {
   return {
      restrict: 'E',
      templateUrl: '/partials/search_bar.html',
      controller: 'searchCtrl',
      bindToController: true,
      controllerAs: 'vm',
      link: function ($scope) {
         // Set searchText to query parameter 'query' on initial page load
         $scope.searchText = $stateParams.query || '';
      }
   };
});

angular
   .module('tcTours', ['angular-tour'])
   .run(function ($templateCache) {
      var tpl =
         `<div class="tour-tip">
                 <span class="tour-arrow tt-{{ ttPlacement }}"
                     ng-hide="centered"></span>
                 <div class="tour-content-wrapper">
                    <p ng-bind="ttContent"></p>
                    <a ng-click="proceed()"
                        ng-bind="ttNextLabel"
                        class="small button tour-next-tip"></a>
                    <a ng-click="$parent.ignore(); closeTour()"
                        class="small button tour-tip">Ignore</a>
                    <a ng-click="closeTour()"
                        class="tour-close-tip">&times;</a>
                 </div>
              </div>`

      $templateCache.put("tour/tour.tpl.html", tpl);
   })
   .directive('tcTour', function (AuthService, $localStorage, tourConfig, $analytics) {
      // FIXME: temporary workaround, see https://github.com/DaftMonk/angular-tour/issues/83
      tourConfig.animation = false;
      return {
         restrict: 'A',
         scope: {},
         link: function (scope, ele, attr) {
            var tour = attr.tcTour;
            var user = AuthService.getUser();

            // Delete element if user has seen tour
            if (user.tours && user.tours[tour]) {
               ele.remove();
               // use this ONLY WITH ISOLATE SCOPE
               scope.$destroy();
               delete $localStorage[tourStepName];
               return
            }

            scope.done = function () {
               if (!user.tours) user.tours = {};
               user.tours[tour] = true;
               user.$update();
               $analytics.eventTrack("Tour Finished", {
                  tour: tour
               });
            };

            // Keep track of current step in localstorage
            var tourStepName = user.id + "_" + tour + "TourStep";
            scope.currentStep = $localStorage[tourStepName] || 0;
            $analytics.eventTrack("Tour Started", {
               tour: tour,
               step: scope.currentStep
            });

            scope.closed = function () {
               $localStorage[tourStepName] = scope.currentStep;
            };

            scope.ignore = function () {
               if (!user.tours) user.tours = {};
               user.tours[tour] = true;
               user.$update();
               $analytics.eventTrack("Tour Cancelled", {
                  tour: tour,
                  step: scope.currentStep
               });
            }

         },
         templateUrl: function (ele, attr) {
            return '/partials/' + attr.tcTour + '_tour.html';
         }
      };
   });

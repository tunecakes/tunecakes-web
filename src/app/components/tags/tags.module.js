angular
.module('tcTags', [])
.directive('tagHelper', function ($compile) {
   // Tag helper types
   var GENRE = 'genre';
   var EMAIL = 'email';

   // Keycodes
   var BACKSPACE = 8;
   var TAB = 9;
   var ENTER = 13;
   var SPACE = 32;
   var LEFT = 37;
   var UP = 38;
   var RIGHT = 39;
   var DOWN = 40;
   var COMMA = 44;
   var HYPHEN = 45;
   var DELETE = 46;
   var PERIOD = 46; // TODO: need to figure out how to handle this being the same as DELETE
   var ATSIGN = 64;
   var UNDERSCORE = 95;
   var LOWERCASE = _.range(65, 91);
   var UPPERCASE = _.range(97, 123);
   var DEFAULT_ALLOWED = _.union([LEFT,
         UP,
         RIGHT,
         DOWN,
         TAB,
         HYPHEN,
         DELETE
      ],
      LOWERCASE,
      UPPERCASE);

   // Inner-HTML template... if you couldn't tell
   var template =
      `
   <span class="tag-box">
      <span ng-repeat="item in ngModel"
          class="tag">
         <div class="tag-container">
            <a ui-sref="parent.search.results({tags: ['{{item}}']})">
               <strong>{{item}}</strong>
            </a>
            <a ng-click="ngModel.splice($index, 1)"
                ng-show="editMode">&#10005;</a>
         </div>
      </span>
   </span>`;

   function getCaretPosition() {
      if (window.getSelection) {
         // Not Internet Explorer =D
         return window.getSelection().focusOffset;
      } else if (document.selection && document.selection.createRange) {
         // Internet Explorer =[
         return -1;
      }
   }

   function setEndOfContenteditable(contentEditableElement) {
      var range, selection;
      if (document.createRange) //Firefox, Chrome, Opera, Safari, IE 9+
      {
         range = document.createRange(); //Create a range (a range is a like the selection but invisible)
         range.selectNodeContents(contentEditableElement); //Select the entire contents of the element with the range
         range.collapse(false); //collapse the range to the end point. false means collapse to end rather than the start
         selection = window.getSelection(); //get the selection object (allows you to change selection)
         selection.removeAllRanges(); //remove any selections already made
         selection.addRange(range); //make the range you have just created the visible selection
      } else if (document.selection) //IE 8 and lower
      {
         range = document.body.createTextRange(); //Create a range (a range is a like the selection but invisible)
         range.moveToElementText(contentEditableElement); //Select the entire contents of the element with the range
         range.collapse(false); //collapse the range to the end point. false means collapse to end rather than the start
         range.select(); //Select the range (make it the visible selection
      }
   }

   function hideTagElement(tagElement) {
      tagElement.css({
         display: 'none'
      });
   }

   function displayTagElement(tagElement, scope) {
      tagElement.css({
         display: 'block'
      });
   }

   function storeTag(scope, tagElement) {
      var tagContent = tagElement.text();

      if (tagContent.trim() !== '' && scope.ngModel.indexOf(tagContent) === -1) {
         tagContent = tagContent.replace(/^-+|-+$/g, '');
         scope.ngModel.push(tagContent.toLowerCase());
      }

      tagElement.text('');
   }

   function keyHandlerFactory(scope, allowedKeys) {
      return function keyHandler(e) {
         // set dirty state on parent so validation error can show up
         // TODO: do we also need to set 'touched'?
         angular.element(this.parentElement).data().$ngModelController.$setDirty();

         // TODO: Handling keys across browsers is a fucking mess, might want to improve later
         // e.key will become the standard but we'll still need a workaround for other/older browsers
         var code = e.which || e.keyCode;

         if (_.indexOf(allowedKeys, code) > -1) {
            return;
         }

         var tagElement = angular.element(e.target);
         var tagContent = tagElement.text();

         switch (code) {
            case ENTER:
               e.preventDefault();
               storeTag(scope, tagElement);
               break;

               // FIXME: this is not working on Chrome... need to do it on keydown instead
               // http://stackoverflow.com/questions/15222105/prevent-default-tabbing-behavior-in-firefox
               // http://api.jquery.com/bind/ 'Returning false from a handler is equivalent to calling both .preventDefault() and .stopPropagation() on the event object.'
            case BACKSPACE:
               var caretPos = getCaretPosition();
               if (caretPos === 0) {
                  e.preventDefault();
                  var lastTagText = scope.ngModel.pop() || '';
                  scope.$apply(tagElement.text(lastTagText + tagContent));
                  setEndOfContenteditable(tagElement[0]);
               }
               break;

            default:
               e.preventDefault();
         }
      };
   }

   return {
      require: 'ngModel',
      scope: {
         ngModel: '=',
         editMode: '='
      },
      template: template,
      link: function (scope, element, attrs, ngModel) {
         var allowed;
         var tagType = GENRE;
         if (EMAIL in attrs) tagType = EMAIL;

         if (!scope.ngModel) scope.ngModel = [];

         element.css({
            overflow: 'auto'
         });

         // Create element
         var newTagModel = 'newTagModel';
         var newTagElement = angular.element('<div class="newTagElmnt"></div>');

         // Use our other custom directive
         newTagElement.attr('contenteditable', 'true')
            .attr('ng-model', newTagModel)
            .attr('highlight-on-focus', '')
            .attr('clear-default-on-focus', '');
         element.append(newTagElement[0]);
         $compile(newTagElement)(scope);

         if (tagType === GENRE) allowed = DEFAULT_ALLOWED;
         else if (tagType === EMAIL) allowed = _.union([
               HYPHEN,
               PERIOD,
               ATSIGN,
               UNDERSCORE
            ],
            DEFAULT_ALLOWED);

         newTagElement.on('keypress', keyHandlerFactory(scope, allowed));
         newTagElement.bind('blur', function(e) {
            storeTag(scope, newTagElement)
         });

         scope.$watch('editMode', function (newVal, oldVal) {
            if (newVal) {
               displayTagElement(newTagElement);
            } else {
               //storeTag(scope, newTagElement)
               hideTagElement(newTagElement);
            }
         });
      }
   };
});

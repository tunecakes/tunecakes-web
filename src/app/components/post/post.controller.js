angular
.module('tcPost', [])
.controller('newPostCtrl', function($scope, $q, $log, $state, ArtistBlogPostFactory,
   MessageService, ImgurUpload) {
   var BlogPost = ArtistBlogPostFactory($scope.artist.id);
   $scope.post = {};

   $scope.images = {
      main: false,
      other: []
   };

   function getImgurLink() {
      if (!$scope.images.main) return $q.resolve();

      MessageService.success('Saving image...');

      return ImgurUpload.uploadImage($scope.images.main)
      .then(function(res) {
         $log.log('imgur upload success', res)
         return res.data.data.link;
      })
      .catch(function(err) {
         MessageService.error('Failed to upload image. ' + err.data.data.error);
         throw err;
      })
   }

   $scope.toggleTwitter = function() {
      $scope.useTwitter = !$scope.useTwitter;
   }

   $scope.toggleFacebook = function() {
      $scope.useFacebook = !$scope.useFacebook;
   }

   $scope.submit = function() {
      var post = new BlogPost($scope.post);

      if($scope.useTwitter) post.twitter = true;
      if($scope.useFacebook) post.facebook = true;

      getImgurLink()
      .then(function(link) {
         post.image_link = link;
         return post.$save()
      })
      .then(function() {
         $state.go('parent.artist.blog', {id: $scope.artist.id});
      })
      .catch(function(err) {
         MessageService.parseHttpError(err);
      });
   }
})
.directive('tcPostCard', function () {
   return {
      templateUrl: '/partials/post_card.html',
      scope: false
   };
});

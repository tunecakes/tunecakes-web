angular
.module('tcNavbar', [])
.directive('tcNavbar', function ($document, $rootScope, $state, AuthService) {
   return {
      restrict: 'E',
      templateUrl: '/partials/navbar.html',
      scope: true,
      controller: function ($scope) {
         function collapseSearch() {
            $scope.isCollapsed = true;
            //angular.element('#site-container').css('padding-top', '0px');
            //angular.element('#second-menu').css('padding-top', '52px');
         }

         function expandSearch() {
            $scope.isCollapsed = false;
            //angular.element('#site-container').css('padding-top', '50px');
            //angular.element('#second-menu').css('padding-top', '102px');
         }

         $scope.getManagedArtists = AuthService.getManagedArtists;
         $scope.isLoggedIn = AuthService.isLoggedIn;

         $scope.unauthButtons = [{
            label: 'Login',
            state: 'parent.login'
         }, {
            label: 'Register',
            state: 'parent.register'
         }];

         $scope.isCollapsed = true && !$state.includes('parent.search');
         $rootScope.$on('$stateChangeSuccess', function () {
            $scope.isCollapsed = true && !$state.includes('parent.search');

            if($scope.isCollapsed) {
               collapseSearch();
            } else {
               expandSearch();
            }
         });


         $scope.toggleSearchCollapse = function() {
            if($scope.isCollapsed) {
               expandSearch();
            } else {
               collapseSearch()
            }
         }

         angular.element($document).ready(function() {
            if($scope.isCollapsed) {
               collapseSearch();
            } else {
               expandSearch();
            }
         });

         $scope.search = function (searchText) {
            $state.go('parent.search', {
               query: searchText
            }, {
               reload: true
            });
         };
      }
   }
})
.directive('tcNavbarButton', function (NotificationService, AuthService, $state) {
   var buttons = {
      notifications: {
         id: 'notifButton',
         label: 'Fan Services',
         imageUrl: 'img/notification_bell.png',
         scope: {
            getNotifications: NotificationService.getNotifications,
            getUnseen: NotificationService.getUnseen,
            markSeen: NotificationService.markSeen
         }
      },
      artists: {
         id: 'artistButton',
         label: 'My Artists',
         imageUrl: 'img/mlogom.png'
      },
      post: {
         id: 'postButton',
         label: 'Create a new artist post',
         imageUrl: 'img/post.png',
         scope: {
            goToEntityCreate: function (artistId, state) {
               this.btn.isopen = false
               $state.go('parent.artist.' + state, {
                  id: artistId
               })
            },
            collapse: {},
            collapseAll: function collapseAll() {
               this.collapse.post = true;
               this.collapse.event = true;
               this.collapse.release = true;
            },
            clickPost: function () {
               if (this.getManagedArtists().length == 1) {
                  this.goToEntityCreate(this.getManagedArtists()[0].id, 'newpost');
               } else {
                  this.toggleCollapse('post');
               }
            },
            clickEvent: function () {
               if (this.getManagedArtists().length == 1) {
                  this.goToEntityCreate(this.getManagedArtists()[0].id, 'newevent');
               } else {
                  this.toggleCollapse('event');
               }
            },
            clickRelease: function () {
               if (this.getManagedArtists().length == 1) {
                  this.goToEntityCreate(this.getManagedArtists()[0].id, 'newrelease');
               } else {
                  this.toggleCollapse('release');
               }
            },
            toggleCollapse: function (type) {
               var isCollapsed = !this.collapse[type];
               this.collapseAll();
               this.collapse[type] = isCollapsed;
            }
         }
      },
      settings: {
         id: 'settingsButton',
         label: 'Settings',
         imageUrl: 'img/test.png',
         dropdown: [
            {
               label: 'Subscribed Artists',
               state: 'parent.subscribed_artists'
            }, {
               label: 'Account Settings',
               state: 'parent.settings'
            }, {
               label: 'Create Artist Profile',
               state: 'parent.new_artist'
            }, {
               label: 'Logout',
               state: 'parent.logout'
            }
         ]
      }
   }

   buttons.post.scope.collapseAll();

   return {
      restrict: 'E',
      templateUrl: '/partials/navbar_button.html',
      transclude: true,
      scope: true,
      controller: function ($scope, $element, $attrs, $transclude) {
         $scope.btn = buttons[$attrs['button']];

         $scope.btn = buttons[$attrs['button']];

         _.extend($scope, $scope.btn.scope);

         $transclude($scope, function (clone, innerScope) {
            $element.find('.dropdown-menu')
               .append(clone);
         });
      }
   }
});

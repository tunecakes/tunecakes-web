angular
.module('tcPasswordReset', [])
.config(function ($stateProvider) {
   $stateProvider
      .state('parent.password_reset', {
         url: '/password_reset',
         templateUrl: '/partials/password_reset.html',
         controller: 'passwordResetCtrl',
         controllerAs: 'vm',
         access: {
            requiredLogin: false
         }
      })
      .state('parent.password_reset_finalize', {
         url: '/password_reset/:email?token',
         templateUrl: '/partials/password_reset_finalize.html',
         controller: 'passwordResetCtrl',
         controllerAs: 'vm',
         access: {
            requiredLogin: false
         }
      })
})
.factory('PasswordReset', function ($http, TunecakesConfig) {
   function initReset(email) {
      return $http.post(TunecakesConfig.apiAddress + '/api/user/' + email + '/password_reset')
   }

   function finalizeReset(email, token, password) {
      var url = TunecakesConfig.apiAddress + '/api/user/' + email + '/password_reset/' + token;
      return $http.post(url, {
         password: password
      });
   }

   return {
      initReset: initReset,
      finalizeReset: finalizeReset
   }

})
.controller('passwordResetCtrl', function (PasswordReset, $stateParams, MessageService) {
   function handleInit() {
      MessageService.success('Password reset sent to ' + this.email +
         '. You will receive an email shortly.');
      this.email = '';
   }

   function handleFinalize() {
      MessageService.success(
         'Congratulations! Your password has been reset. Please try logging in with your new password.'
      );
      this.password = '';
   }

   function handleFail() {
      MessageService.error(
         'Something went wrong while trying to reset your password. Please try again.'
      )
   }

   this.reset = function reset() {
      PasswordReset.initReset(this.email)
         .then(handleInit.bind(this), handleFail.bind(this));
   }

   this.finalizeReset = () => {
      PasswordReset.finalizeReset($stateParams.email, $stateParams.token, this.password)
         .then(handleFinalize.bind(this), handleFail.bind(this));
   }
});

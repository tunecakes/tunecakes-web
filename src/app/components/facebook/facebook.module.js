angular
.module('tcFacebook', [])
.config(function configFBRoutes($stateProvider, TunecakesConfig) {
   try {
      FB;
   } catch(err) {
      setTimeout(function() {
         configFBRoutes($stateProvider, TunecakesConfig)
      }, 500)

      return console.log('Facebook SDK not loaded. Retrying FB module config in 500 ms.');
   }

   FB.init({
      appId: TunecakesConfig.facebook_app_id,
      version: 'v2.9',
      cookie: true
   });

   $stateProvider
   .state('parent.artist.facebook', {
      abstract: true,
      resolve: {
         ensureLoggedIn: function (FacebookService) {
            return FacebookService.getStatus()
            .then(function() {
               return true;
            })
            .catch(function() {
               return FacebookService.login()
            });
         }
      },
      url: '/facebook',
      template: '<div ui-view></div>',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.artist.facebook.link', {
      url: '/link',
      templateUrl: '/partials/artist_social_link.html',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.artist.facebook.posts', {
      url: '/posts',
      controller: 'tcArtistFacebookFeedCtrl',
      controllerAs: 'vm',
      templateUrl: '/partials/artist_import_facebook_feed.html',
      access: {
         requiredLogin: true
      }
   });
   
   console.log('Facebook module configured');
})
.factory('FacebookFeedService', function($q) {

   function FacebookFeedService(pageId, since) {
      var self = this;

      self.getFeed = function getFeed(pageId, pageLimit, offset, since) {
         var query = {
            limit: self.itemsPerPage,
            offset: self.currentOffset,
            since: self.since
         };

         return $q(function(resolve, reject) {
            FB.api('/'+self.pageId+'/posts', query, function(response) {
               if(!response) return reject('No response from FB /'+self.pageId+'/feed');
               if(response.error) return reject(response.error);
               if(response.data.length < 1) {
                  console.log('No data from Facebook page feed');

                  if(self.currentPage !== 0) self.currentPage --;
                  self.currentOffset = self.currentPage * self.itemsPerPage;
               }

               response.data.forEach(function(post, idx) {
                  if(!post.message) {
                     response.data.splice(idx,1);
                  }
               });

               console.log('Facebook Page Posts', response.data);
               resolve(response.data);
            });
         });
      };

      self.pageId = pageId;
      self.since = since;
      self.currentPage = 0;
      self.currentOffset = 0;
      self.itemsPerPage = 25;
      
      self.feedPromise = self.getFeed(self.pageId, self.itemsPerPage, self.currentOffset, self.since);
   }

   FacebookFeedService.prototype.nextPage = function nextPage() {
      this.currentPage ++;
      this.currentOffset = this.currentPage * this.itemsPerPage;

      return this.feedPromise = this.getFeed(this.pageId, this.itemsPerPage, this.currentOffset, this.since);
   };

   FacebookFeedService.prototype.prevPage = function prevPage() {
      this.currentPage --;
      this.currentOffset = this.currentPage * this.itemsPerPage;

      return this.feedPromise = this.getFeed(this.pageId, this.itemsPerPage, this.currentOffset, this.since);
   };

   return FacebookFeedService;

})
.factory('FacebookService', function($q, $http, TunecakesConfig) {
   var isLoggedIn = false,
      pages;

   function handleFbError(error) {
      console.log('Facebook Error', error);
      throw new Error(error);
   }

   function login(cb) {
      return $q(function(resolve, reject) {
         FB.login(function(response) {
            if (response.authResponse) {
               console.log('Successfully logged into Facebook!');
               isLoggedIn = true;
               return resolve(isLoggedIn);
            } else {
               console.log('Error occurred during Facebook log in');
               return reject(isLoggedIn);
            } 
         }, {scope: 'manage_pages,publish_pages,pages_show_list'});
      });
   }

   function getStatus() {
      return $q(function(resolve, reject) {
         console.log('Trying to login to FB')
         FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
               console.log('Facebook is already logged in', response);
               resolve(response);
            } else {
               console.log('Facebook is not logged in');
               reject(response);
            }
         });
      });
   }

   function getUserToken() {
      return getStatus().then(function(response) {
         return response.authResponse.accessToken;
      }, function() {
         throw null;
      });
   }

   function artistLogin(artistId, pageId) {
      return getUserToken().then(function(token) {
         var query = {
            facebook_id: pageId,
            access_token: token
         };

         return $http.post(TunecakesConfig.apiAddress + '/api/artist/' + artistId + '/facebook/login', query);
      });
   }

   function getPages() {
      return $q(function(resolve, reject) {
         FB.api('/me/accounts', 'get', {fields: 'id,name,picture'}, function(response) {
            if(!response) handleFbError('No response from FB /me/accounts');
            if(response.error) handleFbError(response.error);
            
            console.log('Facebook Pages', response);
            resolve(response.data);
         });
      });
   }

   function getPageToken(pageId) {
      return $q(function(resolve, reject) {
         FB.api('/' + pageId, 'get', {fields: 'access_token'}, function(response) {
            if(!response) handleFbError('No response from FB /me/accounts');
            if(response.error) handleFbError(response.error);
         
            resolve(response.access_token);
         });
      });
   }

   function getUserFeed() {
      return $q(function(resolve, reject) {
         FB.api('/me/feed', function(response) {
            if(!response) handleFbError('No response from FB /me/feed');
            if(response.error) handleFbError(response.error);
         });
      });
   }

   function getEvents() {
   
   }

   return {
      login: login,
      artistLogin: artistLogin,
      getStatus: getStatus,
      getUserToken: getUserToken,
      getPages: getPages,
      getPageToken: getPageToken,
      getUserFeed: getUserFeed
   };
})
.controller('facebookLoginCtrl', function($scope, FacebookService) {
   var self = this;

   function setPages() {
      FacebookService.getPages().then(function(pages) {
         self.pages = pages;
      });
   }
   
   FacebookService.getStatus().then(function() {
      self.isLoggedIn = true;
      setPages();
   });

   self.isLoggedIn = false;
   self.login = function() {
      FacebookService.login().then(function() {
         self.isLoggedIn = true;
         setPages();
      });
   };
})
.controller('facebookArtistCtrl', function(FacebookService) {
   var self = this;
   console.log('Entering artist ctrl');

   function linkArtist(fbId) {
      console.log('Linking artist to facebook', self.artist.name, fbId);
      FacebookService.artistLogin(self.artist.id, fbId)
      .then(function(res) {
         console.log('FB artist data', res.data);
         self.artist = res.data;
         //self.artist.facebook_id = fbId;
         //self.artist.facebook = {};
         //self.artist.facebook.access_token = token;
         //self.artist.$update();
      });
   }

   function getFeed() {
      FacebookService.getPageFeed(self.artist.facebook_id);
   }

   FacebookService.getStatus().then
   FacebookService.getPages().then(function(pages) {
      self.pages = pages;
   });

   self.linkArtist = linkArtist;
   self.getFeed = getFeed;
});

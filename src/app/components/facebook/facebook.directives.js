angular
.module('tcFacebook')
.directive('tcFacebookFanLink', function() {
   return {
      restrict: 'E',
      templateUrl: '/partials/fan_link.html',
      scope: {},
      controller: 'facebookCtrl',
      controllerAs: 'vm',
      bindToController: {
         isLoggedIn: '&'
      }
   };
})
.directive('tcFacebookArtistLink', function() {
   return {
      restrict: 'E',
      templateUrl: '/partials/facebook_artist_link.html',
      controller: 'facebookArtistCtrl',
      controllerAs: 'vm',
      bindToController: {
         artist: '='
      }
   };
});

angular
.module('tcImgur', [])
.factory('ImgurUpload', function($http) {
   //var apiUrl = 'https://api.imgur.com/3/upload.json',
   var apiUrl = 'https://api.imgur.com/3/image',
      clientId = '4b3219e90bf42ab',
      headers = {
         'Authorization': 'Client-ID ' + clientId
      };

   function uploadImage(image) {
      return $http.post(apiUrl, {'image': image}, {
         external: true,
         headers: headers
      });
   }

   function uploadManyImages(images) {
      return images.map(function(img) {
         return uploadImage(img);
      });
   }

   return {
      uploadImage: uploadImage,
      uploadManyImages: uploadManyImages
   };
});

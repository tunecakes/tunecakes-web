angular
.module('tcRegister')
.controller('registerCtrl', function ($scope, $log, AuthService, UserFactory, $state,
   $stateParams, MessageService, $analytics) {
   $scope.user = {};
   $scope.user.email = $stateParams.email;
   $scope.UserFactory = UserFactory;

   $scope.submit = function () {
      UserFactory.save($scope.user, function (user) {
         $analytics.setAlias(user.id);
         $analytics.eventTrack("Register");
         AuthService.login({
            email: user.email,
            password: $scope.user.password
         }, user).then(function (user) {
            MessageService.successAfterRedirect(
               $state.go('parent.post_registration', {}, {reload: true}),
               'Registered successfully!'
            );
         });
      },
      function (httpResponse) {
         //TODO: handle failure
         MessageService.parseHttpError(httpResponse);
      });
   };
});

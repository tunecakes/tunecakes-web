angular
.module('tcRegister', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.register', {
      url: '/register?email',
      templateUrl: '/partials/register.html',
      controller: 'registerCtrl',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.post_registration', {
      url: '/post_registration',
      templateUrl: '/partials/post_registration.html',
      access: {
         requiredLogin: true
      }
   });
});

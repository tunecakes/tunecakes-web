angular
.module('tcTwitter')
.controller('twitterAuthCtrl', function($stateParams, artist, TwitterOAuthService) {
   TwitterOAuthService.authorize();
})
.controller('twitterCallbackCtrl', function ($state, $stateParams, artist, TwitterOAuthService) {
   this.artist = artist;
   this.message = 'Saving Twitter settings... One second please! =]';
   
   TwitterOAuthService.sendOAuthVerifier(artist.id, $stateParams.oauth_verifier, $stateParams.oauth_token)
   .then(function() {
      $state.go('parent.artist.social', {}, { reload: true });
   }, (function() {
      this.message = 'There has been a failure while trying to save Twitter settings. Please try again. If this keeps failing, contact us, and we will fix the problem. Sorry about that =[ We don\'t want to cause trouble';
   }).bind(this));
});


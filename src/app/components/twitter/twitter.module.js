angular
.module('tcTwitter', [])
.config(function ($stateProvider) {
   $stateProvider
      .state('parent.artist.twitter', {
         template: '<div ui-view></div>'
      })
      .state('parent.artist.twitter.callback', {
         url: '/twitter/callback?oauth_token&oauth_verifier',
         templateUrl: '/partials/twitter_callback.html',
         controller: 'twitterCallbackCtrl',
         controllerAs: 'vm',
         access: {
            requiredLogin: false
         }
      });
})
.factory('TwitterOAuthService', function ($window, $http, TunecakesConfig) {
   function authorize(artistId) {
      // Open in this tab
      $http.get(TunecakesConfig.apiAddress + '/api/artist/' + artistId + '/twitter/authorize')
         .then(function (res) {
            $window.location.href = res.data.twitter_url
         }).catch(function (res) {
            console.error('Failed to auth Twitter', res)
         });
   }

   function sendOAuthVerifier(artistId, oauthVerifier, oauthToken) {
      var body = {
         oauth_verifier: oauthVerifier,
         oauth_token: oauthToken
      }
      return $http.post(TunecakesConfig.apiAddress + '/api/artist/' + artistId +
            '/twitter/callback', body)
         .then(function twitterOAuthSuccess(res) {
            return res.data;
         }).catch(function twitterOAuthFailure(res) {
            throw res;
         });
   }

   return {
      authorize: authorize,
      sendOAuthVerifier: sendOAuthVerifier
   };
});

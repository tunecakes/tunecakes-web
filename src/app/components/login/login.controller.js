angular
.module('tcLogin')
.controller('loginCtrl', function ($scope, AuthService, $log, $state, $stateParams, MessageService) {
   $scope.submit = function () {
      AuthService.login($scope.user).then(function () {
        // Check for parameters to determine location user should go to after login
        var afterLogin;
        var params = {};
        if ($stateParams.prevState) {
          afterLogin = $stateParams.prevState;
          params.id = $stateParams.prevId;
        } else {
          afterLogin = 'parent.feed.events';
        }
        //Send user to the appropriate location
         $state.go(afterLogin, params, {reload: true});
      }, function (httpResponse) {
         MessageService.parseHttpError(httpResponse);
      });
   };
})
.controller('logoutCtrl', function ($state, AuthService, $window, $log) {
   AuthService.logout();
   $state.go('parent.main');
});

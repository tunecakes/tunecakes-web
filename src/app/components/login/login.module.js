angular
   .module('tcLogin', [])
   .config(function ($stateProvider) {
      $stateProvider
         .state('parent.login', {
            url: '/login?prevState&prevId',
            templateUrl: '/partials/login.html',
            controller: 'loginCtrl',
            access: {
               requiredLogin: false
            }
         })
         .state('parent.logout', {
            url: '/logout',
            controller: 'logoutCtrl'
         })
   });

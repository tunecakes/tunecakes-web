angular
.module('tcFeed')
.controller('nearbyArtistCtrl', function ($scope, $window, user, $log, SearchService) {
   function updateResults(results) {
      console.log('Search results found', results);
      $scope.nearbyArtists = results.artists;
   }

   var QueryBuilder = SearchService('feed');

   $scope.user = user;
   $scope.nearbyArtists = [];

   if ($scope.user.location && $scope.user.location.lat && $scope.user.location.lon) {
      QueryBuilder.setSearchArea($scope.user.location.lat, $scope.user.location.lon);
   }

   QueryBuilder.setRandom(true);
   QueryBuilder.onResults(updateResults);
   QueryBuilder.setFilter('artist');
   QueryBuilder.executeQuery();
})
.controller('feedCtrl', function ($scope, $q, user, feed, $http, TunecakesConfig) {
   $scope.user = user;
   $scope.feed = feed;

   console.log("FEED", feed)
})
.directive('tcFeedCard', function ($http, TunecakesConfig) {
   return {
      priority: 1001,
      templateUrl: '/partials/feed_card.html',
      scope: {
         feedItem: '='
      },
      link: function (scope) {
         let item = scope.feedItem;
         $http.post(TunecakesConfig.apiAddress + '/api/' + item.actor_type + '/' + item.actor + '/transform_feed_item', item)
         .then(res => {
            scope.activity = res.data
         }).catch(err => {
            console.log('Failed to generate feed card for item', item, err);
         });
      }
   };
});

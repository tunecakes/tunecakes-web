angular
.module('tcFeed', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.feed', {
      url: '/feed',
      abstract: true,
      views: {
         '': {
            template: `
            <ui-view></ui-view>
            `
         },
         'second-menu@': {
            template: `
            <div class="artnavitem">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   id="feed_events_button"
                   ui-sref="parent.feed.events"
                   ui-sref-active="active">
                  Events
               </button>
            </div>
            <div class="artnavitem artnavitem-extended">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   id="feed_artists_button"
                   ui-sref="parent.feed.artists"
                   ui-sref-active="active">
                  Local Artists
               </button>
            </div>
            <div class="artnavitem">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   id="feed_activities_button"
                   ui-sref="parent.feed.activities"
                   ui-sref-active="active">
                  Feed
               </button>
            </div>
            `
            //<div class="artnavitem artnavitem-extended">
            //   <button type="button"
            //       class="col-xs-12 btn btn-primary"
            //       id="feed_artists_button"
            //       ui-sref="parent.feed.sampler"
            //       ui-sref-active="active">
            //      Stage Sampler
            //   </button>
            //</div>
         }
      },
      access: {
         requiredLogin: true
      }
   })
   .state('parent.feed.artists', {
      url: '/artists',
      templateUrl: '/partials/artists_feed.html',
      controller: 'nearbyArtistCtrl',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.feed.events', {
      url: '/events',
      template: '<tc-event-calendar location="$resolve.user.location"></tc-event-calendar>',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.feed.sampler', {
      url: '/sampler',
      templateUrl:'/partials/crystal_ball.html',
      controller:'tcCrystalBallCtrl',
      controllerAs:'vm',
      bindToController: true,
      access: {
         requiredLogin: true
      }
   })
   .state('parent.feed.activities', {
      url: '/activities',
      templateUrl: '/partials/activity_feed.html',
      resolve: {
         feed: function (FeedFactory, user, resolved) {
            return FeedFactory('user', 'aggregated').get({id: user.id})
            .$promise.catch(function(err) {
               console.log('Error grabbing feed data', err);
               return [];
            });
         }
      },
      controller: 'feedCtrl',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.feed.feedcard', {
      url: '/feedcard',
      templateUrl: '/partials/feed_card.html'
   });
})
.factory('FeedFactory', function ($resource, TunecakesConfig) {
   var FeedFactory = function FeedFactory(resource, type) {
      return $resource(
         TunecakesConfig.apiAddress + '/api/' + resource + '/:id/feed/' + type, {
            id: '@id',
            type: '@type'
         }
      )
   }
   return FeedFactory;
})
.factory('AggregatedFeedFactory', function ($resource, TunecakesConfig) {
   return function(id, type, feedName) {
      var uri = TunecakesConfig.apiAddress + '/api/' + type + '/:id/feed/' + feedName;
      var feed = $resource(uri, {id: '@id', type: '@type'});
      
      return feed.get({id: id})
   }
});

angular
.module('tcSpotify', [])
.config(function configFBRoutes($stateProvider, TunecakesConfig) {
   $stateProvider
   .state('parent.spotify', {
      abstract: true,
      url: '/spotify',
      template: '<ui-view/>'
   })
   .state('parent.spotify.link', {
      url: '/link',
      template: '<h3>Redirecting you to Spotify account link page...</h3>',
      controller: function($state, SpotifyService, user, MessageService) {
         if(user.spotify && user.spotify.access_token) {
            $state.go('parent.spotify.import')
         } else {
            SpotifyService.redirectToSpotify(user.id)
            .catch(err => {
               MessageService.error('Failed to get Spotify redirect. Please try again.');
            });
         }
      },
      bindToController: true,
      controllerAs: 'vm',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.spotify.relink', {
      url: '/relink',
      template: '<h3>Redirecting you to Spotify account link page...</h3>',
      controller: function(SpotifyService, user, MessageService) {
         SpotifyService.redirectToSpotify(user.id)
         .catch(err => {
            MessageService.error('Failed to get Spotify redirect. Please try again.');
         });
      },
      bindToController: true,
      controllerAs: 'vm',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.spotify.callback', {
      url: '/callback?code&error',
      template: `
         <div class="col-xs-12">
            <h5>{{vm.message}}</h5>
         </div>
         <div class="col-xs-12 text-center" ng-if="vm.error">
            <a ui-sref="parent.spotify.link">Go To Spotify Link Page</a>
         </div>
      `,
      controller: 'spotifyAuthCtrl',
      bindToController: true,
      controllerAs: 'vm',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.spotify.import', {
      url: '/import',
      template: `
         <panel>
            <h3>Exact Artist matches found from your Spotify Follow List</h3>
            <h5>These artists were matched exactly using the Spotify ID</h5>
            <div ng-repeat="artist in vm.tcArtists">
               <tc-artist-card artist="artist"></tc-artist-card>
            </div>
         </panel>
         <panel>
            <h3>Possible Artist matches from your Spotify Follow List</h3>
            <h5>
               These artists are a best guess match. We use the artist name given by Spotify
               and search our database for an artist with a similar name
            </h5>

            <div ng-repeat="maybe in vm.maybeMatches">
               <tc-artist-card artist="maybe.match"></tc-artist-card>
               <div>
                  Spotify Name: <i>{{maybe.artist.name}}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                  Spotify ID: <i>{{maybe.artist.id}}</i>
               </div>
            </div>
         </panel>
         <panel ng-if="vm.artistsNotFound.length">
            <h3>Artists not on Tunecakes</h3>
            <h5>These artists were added to a list that let's us know what artists to add to Tunecakes</h5>
            <table class="table">
               <thead>
                  <tr>
                     <th>Artist Name</th>
                     <th>Spotify ID</th>
                  </tr>
               </thead>
               <tbody>
                  <tr ng-repeat="artist in vm.artistsNotFound">
                     <td>{{artist.name}}</td>
                     <td>{{artist.id}}</td>
                  </tr>
               </tbody>
            </table>
         </panel>
         <panel>
            <h3>Your Spotify Follow List</h3>
            <table class="table">
               <thead>
                  <tr>
                     <th>Artist Name</th>
                     <th>Spotify ID</th>
                  </tr>
               </thead>
               <tbody>
                  <tr ng-repeat="artist in vm.spotifyArtists">
                     <td>{{artist.name}}</td>
                     <td>{{artist.id}}</td>
                  </tr>
               </tbody>
            </table>
         </panel>
      `,
      controller: 'tcSpotifyImportCtrl',
      bindToController: true,
      controllerAs: 'vm',
      access: {
         requiredLogin: true
      }
   });
})
.factory('SpotifyService', function($http, $window, TunecakesConfig) {
   function redirectToSpotify(userId) {
      return $http.get(TunecakesConfig.apiAddress + '/api/user/' + userId + '/spotify')
      .then(function (res) {
         $window.location.href = res.data.url
      });
   }

   function saveAuthCode(userId, code) {
      return $http.post(TunecakesConfig.apiAddress + '/api/user/' + userId + '/spotify', {code});
   }

   function getFollowedArtists(userId) {
      return $http.get(TunecakesConfig.apiAddress + '/api/user/' + userId + '/spotify/followed_artists');
   }

   function getArtistsBySpotifyId(spotifyId) {
      return $http.get(TunecakesConfig.apiAddress + '/api/artist', {params: {query: {spotify_id: spotifyId}}});
   }

   function getRequestedArtistBySpotifyId(spotifyId) {
      return $http.get(TunecakesConfig.apiAddress + '/api/request', {params: {query: {spotify_id: spotifyId}}});
   }

   function requestArtistBySpotifyId(artistName, spotifyId) {
      var data = {
         spotify_id: spotifyId,
         artist_name: artistName
      }
      return $http.post(TunecakesConfig.apiAddress + '/api/request', data);
   }

   return {
      redirectToSpotify,
      saveAuthCode,
      getFollowedArtists,
      getArtistsBySpotifyId,
      getRequestedArtistBySpotifyId,
      requestArtistBySpotifyId
   };
});

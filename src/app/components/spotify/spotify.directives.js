angular
.module('tcSpotify')
.directive('tcSpotifyLinker', function() {
   return {
      template: `
      <panel>
         <button ui-sref="parent.spotify.link" class="btn btn-default btn-lg col-xs-12">
            Link artists from your Spotify account
         </button>
      </panel>
      `
   }
})

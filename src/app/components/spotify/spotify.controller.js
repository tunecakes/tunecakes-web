angular
.module('tcSpotify')
.controller('tcSpotifyImportCtrl', function(SpotifyService, SearchService, user) {
   this.spotifyArtists = [];
   this.tcArtists = [];
   this.maybeMatches = [];
   this.artistsNotFound = [];

   this.user = user;

   SpotifyService.getFollowedArtists(this.user.id)
   .then(res => {
      this.spotifyArtists = res.data;

      this.spotifyArtists.forEach((a) => {
         SpotifyService.getArtistsBySpotifyId(a.id)
         .then(res => {
            if(res.data.length) {
               handleFoundArtist(res.data.pop());
            } else {
               findArtistByName(a);
            }
         })
      });
   });

   var addMaybeMatch = (artist, match) => {
      this.maybeMatches.push({artist, match});
   }

   var findArtistByName = (artist) => {
      var QueryBuilder = SearchService('find' + artist.name);

      QueryBuilder.onResults((results) => {
         // Absolutely nothing found
         if(!results.artists.length) return handleNotFoundArtist(artist);

         addMaybeMatch(artist, results.artists[0]._source);
         requestArtist(artist);
      });

      QueryBuilder.setSearchText(artist.name);
      QueryBuilder.setFilter('artist');
      QueryBuilder.executeQuery();
   };

   var handleFoundArtist = (artist) => {
      this.tcArtists.push(artist);
   };

   var handleNotFoundArtist = (artist) => {
      this.artistsNotFound.push(artist);
      requestArtist(artist);
   };

   var requestArtist = (artist) => {
      SpotifyService.getRequestedArtistBySpotifyId(artist.id).then((res) =>{
         if(res.data.length) {
            console.log("Found artist request for spotify artist", artist);
            return;
         }

         SpotifyService.requestArtistBySpotifyId(artist.name, artist.id).then(() =>{
            console.log("Successfully requested spotify artist", artist);
         }).catch(res => {
            console.log("Failed to request spotify artist", artist, res);
         });
      }).catch(res => {
         console.log("Error finding requested artists", res)
      });
   };
})
.controller('spotifyAuthCtrl', function($http, $state, $stateParams, SpotifyService, user, MessageService) {
   this.error = $stateParams.error;
   this.code = $stateParams.code;
   this.user = user;

   if(this.error == 'access_denied') {
      this.message = 'You did not grant Tunecakes access to your Spotify account. ' +
         'If that was a mistake, click below to go back to the Spotify link page and ' +
         'try again. If not, then no problem! We understand.';
      return;
   } else if(this.error) {
      this.message = 'Unknown error returned from Spotify. Please try again by clicking ' +
         'below. If this keeps happening, let us know and give us this error message: ' +
         this.error;
      return;
   } else if(!this.code) {
      this.message = 'Unknown error: No Spotify code returned. Please try again by ' +
         'following the link below or let us know if this keeps happening.';
      this.error = true;
      return;
   }

   this.message = 'Saving Spotify token to account... This page will automatically redirect.';

   SpotifyService.saveAuthCode(this.user.id, this.code)
   .then(() => {
      MessageService.successAfterRedirect(
         $state.go('parent.feed.events'),
         'Successfully save Spotify creds'
      );
   }).catch(err => {
      this.error = err;
      this.message = 'Failed to save Spotify creds. Please try again. Error: ' + this.error;
   });
})

angular
.module('tcRelease')
.directive('tcReleaseCard', function () {
   return {
      templateUrl: '/partials/release_card.html',
      restrict: 'E',
      controller: function ($state, AuthService, ReleaseFactory) {
         this.$onInit = function() {
            if (AuthService.isOwner(this.release) ||
               AuthService.isAdmin(this.release)) {
               this.isAdmin = true;
            }

            this.release = new ReleaseFactory(this.release);
         };
      },
      controllerAs: 'vm',
      scope: {
         release: '='
      },
      bindToController: true,
   };
})
.directive('tcReleaseFormSimple', function () {
   return {
      templateUrl: '/partials/release_form_simple.html',
      scope: false,
      restrict: 'E'
   };
})
.directive('tcPurchaseLinks', function() {
   return {
      scope: {
         links: '='
      },
      controller: function () {
         this.$onInit = function() {
            console.log(this.links);
            this.links = this.links || [];
         }
         
         this.addLink = function addLink() {
            this.links.push({})
         }.bind(this);
      },
      controllerAs: 'vm',
      bindToController: true,
      templateUrl: '/partials/add_purchase_links.html'
   };
})
.directive('tcSongs', function() {
   return {
      scope: {
         songs: '='
      },
      templateUrl: '/partials/release_songs.html'
   };
})
.directive('tcAddSong', function () {
   return {
      scope: {
         songAdded: '&'
      },
      controller: function () {
         this.song = {};

         this.addSong = function addSong() {
            this.songAdded({song: this.song});
            this.song = {};
         }.bind(this);
      },
      controllerAs: 'vm',
      bindToController: true,
      templateUrl: '/partials/add_songs.html'
   };
})
.directive('tcDurationFormat', function ($filter) {
   return {
      require: 'ngModel',
      link: function (scope, ele, attr, ngModel) {
         ngModel.$formatters.push(function (input) {
            return $filter('asMinutes')(input, true);
         });
         ngModel.$parsers.push($filter('toMilliseconds'));
      }
   }
});

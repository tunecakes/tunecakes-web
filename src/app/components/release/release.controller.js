angular
.module('tcRelease')
.controller('newReleaseCtrl', function ($log, $q, AuthService, ReleaseFactory,
   $state, artist, MessageService, ImgurUpload) {
   this.ReleaseFactory = ReleaseFactory;
   this.artist = artist;

   this.release = {
      contributing_artists: [],
      songs: []
   };

   this.images = {
      main: false,
      other: []
   };

   var getImgurLink = () => {
      if (!this.images.main) return $q.resolve();

      MessageService.success('Saving image...');

      return ImgurUpload.uploadImage(this.images.main)
      .then(function(res) {
         $log.log('imgur upload success', res)
         return res.data.data.link;
      }).catch(function(err) {
         $log.error('imgur upload fail', err)
         MessageService.success('Failed to save image, try again later');
         return;
      });
   }

   this.addSong = (song) => {
      console.log('TEST', song)
      this.release.songs.push(song);
   }

   this.submit = () => {
      $log.log("Processing event..");

      var release = new ReleaseFactory(this.release);
      release.artist = this.artist.id;

      release.$save()
      .catch(function(err) {
         MessageService.parseHttpError(err);
      })
      .then(getImgurLink)
      .then(function(link) {
         if(!link) return;

         release.image_link = link;
         return release.$update();
      })
      .catch(function() { return; })
      .then(function() {
         $state.go('parent.release.main', {id: release.id});
      });
   };
})
.controller('releaseCtrl', function ($state, $q, $log, AuthService, ReleaseFactory, ImgurUpload,
   MessageService, SongFactory, release) {
   var Song;
   this.release = release;

   this.$onInit = function() {
      console.log('release', this.release)
      if (AuthService.isOwner(this.release) ||
         AuthService.isAdmin(this.release)) {
         this.isAdmin = true;
      }

      Song = SongFactory(this.release.id);
   };

   this.images = {
      main: false,
      other: []
   };

   var getImageLink = () => {
      if (!this.images.main) return $q.resolve(this.release.image_link);

      MessageService.success('Saving image...');

      return ImgurUpload.uploadImage(this.images.main)
      .then(function(res) {
         $log.log('imgur upload success', res)
         return res.data.data.link;
      });
   }

   this.isAdmin = false;
   this.AuthService = AuthService;
   this.ReleaseFactory = ReleaseFactory;

   this.addSong = function(song) {
      var new_song = new Song(song);
      new_song.$save();

      this.release.songs.push(new_song);
   }.bind(this);

   this.cancel = function () {
      this.release.$get();
   };

   this.save = function () {
      getImageLink()
      .catch(function(err) {
         MessageService.error('Failed to upload image. ' + err.data.data.error);
         throw err;
      }.bind(this))
      .then(function(link) {
         this.release.image_link = link;
         return this.release.$update();
      }.bind(this)).then(function () {
         $state.go('parent.release.main');
      }.bind(this));
   };

   this.delete = function () {
      if (!confirm('Are you sure you want to delete this release?'))
         return;
      this.release.$remove();
      $state.go('parent.feed.events');
   };
})
.controller('songCtrl', function() {

});

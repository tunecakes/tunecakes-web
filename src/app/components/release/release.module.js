angular
.module('tcRelease', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.release', {
      url: '/release/:id',
      abstract: true,
      views: {
         '': {
            templateUrl: '/partials/release.html',
            controller: 'releaseCtrl',
            controllerAs: 'vm',
            bindToController: true
         },
         'second-menu@': {
            template: `
            <div class="artnavitem" ng-show="$resolve.isAdmin">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   ui-sref="parent.release.main"
                   ui-sref-active="active">
                  Info
               </button>
            </div><div class="artnavitem" ng-show="$resolve.isAdmin">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   ui-sref="parent.release.edit"
                   ui-sref-active="active">
                  Edit
               </button>
            </div><div class="artnavitem" ng-show="$resolve.isAdmin">
               <button type="button"
                   class="col-xs-12 btn btn-danger"
                   ng-click="vm.delete()">
                  Delete
               </button>
            </div>
            `,
            controller: 'releaseCtrl',
            controllerAs: 'vm',
            bindToController: true
         }
      },
      resolve: {
         release: function ($stateParams, ReleaseFactory) {
            return ReleaseFactory.get({
               id: $stateParams.id
            }).$promise;
         },
         isAdmin: function(release, AuthService) {
            return AuthService.isOwner(release) || AuthService.isAdmin(release);
         }
      },
      access: {
         requiredLogin: false
      }
   })
   .state('parent.release.main', {
      url: '^/release/:id',
      templateUrl: '/partials/release_main.html',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.release.edit', {
      url: '/edit',
      templateUrl: '/partials/release_edit.html',
      access: {
         requiredLogin: false
      }
   });
})
.factory('ReleaseFactory', function (ResourceFactory) {
   return ResourceFactory('/release/:id', true, true);
})
.factory('SongFactory', function ($resource, TunecakesConfig) {
   return function SongFactory(releaseId) {
      var path = TunecakesConfig.apiAddress + '/api/release/:rid/song/:id'
      return $resource(path, {rid: releaseId, id: '@id'})
   };
})
.factory('ReleaseOwnerFactory', function (SubResourceFactory) {
   return SubResourceFactory('release', 'owner', true);
})
.factory('ReleaseAdminFactory', function (SubResourceFactory) {
   return SubResourceFactory('release', 'admin');
})
.factory('ReleaseArtistFactory', function (SubResourceFactory) {
   return SubResourceFactory('release', 'artist');
});

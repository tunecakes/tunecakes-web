angular
.module('tcMain', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.main', {
      url: '/',
      redirectTo: 'parent.main.calendar',
      views: {
         '': {
            template: `
            <ui-view></ui-view>
            `
         },
         'second-menu@': {
            template: `
            <div class="artnavitem artnavitem-extended">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   id="feed_events_button"
                   ui-sref="parent.main.calendar"
                   ui-sref-active="active">
                  Event Calendar
               </button>
            </div>
            <div class="artnavitem artnavitem-extended">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   id="feed_artists_button"
                   ui-sref="parent.main.register"
                   ui-sref-active="active">
                  Register
               </button>
            </div>
            `
            //<div class="artnavitem artnavitem-extended">
            //   <button type="button"
            //       class="col-xs-12 btn btn-primary"
            //       id="feed_artists_button"
            //       ui-sref="parent.main.sampler"
            //       ui-sref-active="active">
            //      Stage Sampler
            //   </button>
            //</div>
         }
      },
      access: {
         requiredLogin: false
      }
   })
   .state('parent.main.calendar', {
      url: '^/calendar',
      templateUrl: '/partials/main.html'
   })
   .state('parent.main.sampler', {
      url: '^/sampler',
      templateUrl:'/partials/crystal_ball.html',
      controller:'tcCrystalBallCtrl',
      controllerAs:'vm',
      bindToController: true,
   })
   .state('parent.main.register', {
      url: '^/register?email',
      templateUrl: '/partials/register.html',
      controller: 'registerCtrl',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.privacy_policy', {
      url: '/privacy_policy',
      templateUrl: '/partials/privacy_policy.html',
      access: {
         requiredLogin: false
      }
   })
})
.directive('tcScrollDown', function() {
   return {
      controllerAs: 'vm',
      controller: function($window, $document) {
         function isBottom() {
            return $window.scrollY >= $window.scrollMaxY - 50;
         }

         this.atBottom = isBottom()

         angular.element($window).on('scroll', function(e) {
            e.stopPropagation();
            this.atBottom = isBottom();
         }.bind(this));
      },
      template: `
         <footer ng-show="!vm.atBottom"
             id="scrolldownplease">
            <div>
               <span style="font-size: 5em; color: #a285bd;"
                   class="glyphicon glyphicon-circle-arrow-down"></span>
            </div>
         </footer>
      `
   }
});

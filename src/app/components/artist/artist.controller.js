angular
.module('tcArtist')
.controller('artistBlogCtrl', function(posts) {
   this.posts = posts;
})
.controller('newArtistCtrl', function() {
   this.newArtist = {
      admins: [],
      members: [],
      tags: [],
      location: {}
   }
})
.controller('artistFollowerCtrl', function (ArtistFollowerFactory, AuthService, $analytics) {
   this.menuCollapse = true;
   this.toggleMenu = function () {
      this.menuCollapse = !this.menuCollapse;
   };

   this.loggedIn = AuthService.isLoggedIn()

   // Follow flags
   this.events = true;
   this.releases = true;
   this.blog = false;
   this.other = false;

   this.toggleEventFlag = function () {
      this.events = !this.events;
      this.update();
   };
   this.toggleReleaseFlag = function () {
      this.releases = !this.releases;
      this.update();
   };
   this.toggleBlogFlag = function () {
      this.blog = !this.blog;
      this.update();
   };
   this.toggleOtherFlag = function () {
      this.other = !this.other;
      this.update();
   };

   function getFlags() {
      var flags = [];
      if (this.events) flags.push('events');
      if (this.releases) flags.push('releases');
      if (this.blog) flags.push('blog');
      if (this.other) flags.push('other');

      return flags;
   }

   function handleFollower(follower) {
      this.follower = follower

      if (follower.flags.indexOf('events') >= 0) {
         this.events = true;
      } else {
         this.events = false;
      }

      if (follower.flags.indexOf('releases') >= 0) {
         this.releases = true;
      } else {
         this.releases = false;
      }

      if (follower.flags.indexOf('blog') >= 0) {
         this.blog = true;
      } else {
         this.blog = false;
      }

      if (follower.flags.indexOf('other') >= 0) {
         this.other = true;
      } else {
         this.other = false;
      }
   }

   this.$onInit = () => {
      ArtistFollowerFactory.get({
            id: this.artist.id
         })
         .$promise.then(handleFollower.bind(this))
         .catch(function () {
            this.follower = null;
         }.bind(this));
      console.log('follower', this.follower);
   };

   this.follow = function () {
      // might be better if we can add this to the resource factory
      $analytics.eventTrack("Follow Artist", {
         artist: this.artist.id,
         flags: getFlags.bind(this)()
      });
      ArtistFollowerFactory.follow({
         id: this.artist.id
      }, {
         flags: getFlags.bind(this)()
      }).$promise.then(handleFollower.bind(this));
   };

   this.update = function () {
      this.follower.flags = getFlags.bind(this)();
      this.follower.$updateFollower({
         id: this.artist.id
      });
   };

   this.unfollow = function () {
      this.follower.$unfollow({
         id: this.artist.id
      });
      this.follower = null;
   };
})
.controller('artistSimpleCtrl', function (ArtistFollowerFactory, AuthService,
   $analytics) {
   this.addFollower = function () {
      if (!this.AuthService.isLoggedIn()) return;
      // might be better if we can add this to the resource factory
      $analytics.eventTrack("Follow Artist", {
         artist: this.artist.id,
         flags: ['releases', 'events']
      });

      this.isFollowing = true;
      ArtistFollowerFactory.follow({
         id: this.artist.id
      }, {
         flags: ['releases', 'events']
      }, function () {});
   };

   this.removeFollower = function () {
      if (!this.AuthService.isLoggedIn()) return;
      // might be better if we can add this to the resource factory
      $analytics.eventTrack("Unfollow Artist", {
         artist: this.artist.id
      });

      this.isFollowing = false;
      ArtistFollowerFactory.unfollow({
         id: this.artist.id
      }, function () {});
   };

   this.toggleFollower = function () {
      if(this.isFollowing) {
         this.removeFollower();
      } else {
         this.addFollower();
      }
   };

   this.$onInit = () => {
      this.isInit = false
      this.isFollowing = false;
      this.AuthService = AuthService;

      ArtistFollowerFactory.get({
         id: this.artist.id
      }).$promise.then(follower => {
         console.log('FOllower', follower)
         if (follower && follower.flags) {
            this.isFollowing = true
         }
         this.isInit = true;
      }).catch(error => {
         this.isInit = true;
         console.log('Get follower error', error)
      });
   }
})
.controller('artistFormCtrl', function ($analytics, $q, $state, ArtistAPI, ImgurUpload, MessageService, NoembedService) {
   var artistData = {};
   this.NoembedService = NoembedService;
   this.images = {}

   let setImageLink = () => {
      if (!this.images.main) return $q.resolve();

      MessageService.success('Uploading image...');

      return ImgurUpload.uploadImage(this.images.main)
      .then((res) => {
         console.log('imgur upload success', res);
         artistData.image_link = res.data.data.link;
      })
      .catch(err => {
         console.log("Failed to upload artist image to Imgur", err)

         if(!err || !err.data) throw err;

         if(!err.data.data) throw err.data;

         if(!err.data.data.error) throw err.data.data;

         throw err.data.data.error;
      });
   }

   let setArtistData = () => {
      for(let key in this.form) {
         // Skip the angular properties
         if(key.startsWith("$")) continue;

         // Only use use it if it's dirty ;]
         if(this.form[key].$pristine) continue;

         artistData[key] = this.artist[key];
      }

      artistData.members = this.artist.members;
      artistData.admins = this.artist.admins;
      artistData.tags = this.artist.tags;
      artistData.location = this.artist.location;
   }

   this.save = () => {
      setArtistData();

      setImageLink()
      .catch(function (err) {
         if(confirm("Failed to upload the image you gave us. Would you like to continue saving the artist without the image?")) {
            return;
         }

         MessageService.error('Failed to upload image. ' + err);
         throw err;
      })
      .then(() => {
         if(this.artist.id) {
            $analytics.eventTrack('Artist Info Updated');
            return ArtistAPI.update(this.artist.id, artistData);
         } else {
            $analytics.eventTrack('Artist Created');
            return ArtistAPI.create(artistData)
         }
      }).then(function (artist) {
         $state.go('parent.artist.profile', {id: artist.id}, {reload: true});
      })
      .catch((err) => {
         console.log("Artist save error", err)
      });
   };
})
.controller('artistCtrl', function ($scope, $state, artist, isAdmin, ArtistAPI, AuthService,
   NoembedService, MessageService, FeedFactory, TwitterOAuthService) {

   $scope.AuthService = AuthService;
   $scope.feed = FeedFactory('artist', 'everything').get({
      id: artist.id
   });
   $scope.authorizeTwitter = TwitterOAuthService.authorize;

   /*
    * Other Artist Controller stuff
    */
   $scope.artist = artist;
   $scope.isAdmin = isAdmin;
   $scope.NoembedService = NoembedService;

   $scope.delete = function () {
      var confirmDelete = confirm('Are you sure you want to delete this Artist?');
      if (confirmDelete) {
         ArtistAPI.delete(artist.id).then(() => {
            AuthService.updateUser();
            $state.go('parent.feed.events');
         }).catch(() => {
            MessageService.error("Failed to delete this artist. Please try again or let us know if this keeps happening.")
         });
      }
   };
})
.controller('artistEventsCtrl', function (artist, events) {
   this.artist = artist;
   this.events = events;

   this.leaveEvent = function (event) {
      var confirmDelete = confirm('Are you sure you want to leave this event?');
      if (confirmDelete) {
         event.$delete();
      }
   };
})
.controller('artistReleasesCtrl', function (artist, releases, ArtistAPI) {
   this.artist = artist;
   let getMoreReleases = page => {
      ArtistAPI.getReleases(artist.id, page).then(releases => {
         if(!releases || !releases.length) return;

         this.releases= this.releases.concat(releases);
         getMoreReleases(++page);
      });
   };

   ArtistAPI.getReleases(artist.id).then(releases => {
      this.releases = releases;
      getMoreReleases(1);
   });
})
.controller('artistSettingsCtrl', function ($scope, $log, $state,
   ArtistOwnerFactory, ArtistAdminFactory, EventArtistFactory) {
   $scope.addAdmin = function () {
      ArtistAdminFactory.save({
         id: $scope.artist.id,
         subId: $scope.newAdmin
      }, function () {
         $scope.artist.$get();
      });
   };

   $scope.removeAdmin = function (adminId) {
      ArtistAdminFactory.delete({
         id: $scope.artist.id,
         subId: adminId
      }, function () {
         $scope.artist.$get();
      });
   };

   $scope.changeOwner = function (ownerId) {
      ArtistOwnerFactory.update({
         id: $scope.artist.id,
         subId: ownerId
      }, function () {
         $scope.artist.$get();
         $state.go('parent.artist.profile', {
            id: $scope.artist.id
         });
      });
   };

   $scope.leaveEvent = function (eventId) {
      EventArtistFactory.delete({
         id: eventId,
         subId: $scope.artist.id
      }, function () {
         $scope.artist.$get();
      });
   };
})
.controller('tcArtistFacebookFeedCtrl', function ($log, artist, ArtistBlogPostFactory,
   FacebookFeedService) {
   var self = this,
      BlogPost = ArtistBlogPostFactory(artist.id),
      feedService = new FacebookFeedService(artist.facebook_id),
      states = ['loading', 'ready', 'importing'];

   feedService.feedPromise.then(function (data) {
      self.state = states[1];
      self.feed = data;
   });

   self.state = states[0];

   self.nextPage = function () {
      self.state = states[0];
      feedService.nextPage().then(function (data) {
         self.state = states[1];
         self.feed = data;
      });
   };

   self.import = function () {
      self.state = states[2];
   }

   self.saveFacebookPost = function (id, message) {
      $log.log(id, message);
      var post = new BlogPost({
         message: message,
         facebook_id: id
      });

      post.$save();
   }

   self.populateActivePosts = function () {
      self.feed.forEach(function (item) {
         self.saveFacebookPost(item.id, item.message);
      });
   }
});

angular
.module('tcArtist', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.new_artist', {
      url: '/new_artist',
      controller: 'newArtistCtrl',
      controllerAs: 'vm',
      bindToController: true,
      templateUrl: '/partials/new_artist.html',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.subscribed_artists', {
      url: '/subscribed_artists',
      template: `
      <h3>Artists you are following</h3>
      <panel>
         <tc-artist-card ng-repeat="artist in vm.artists" artist="artist"></tc-artist-card>
      </panel>
      `,
      resolve: {
         subscriptions: function(user, MessageService, ArtistSubscriptions) {
            return ArtistSubscriptions.getAll(user.id)
            .catch(err => {
               MessageService.error('Failed to get user subscriptions');
               console.log('Subscriptions error', err);
               return [];
            });
         }
      },
      controller: function(subscriptions, ArtistAPI) {
         this.artists = [];
         subscriptions.forEach(sub => {
            ArtistAPI.findById(sub.publisher)
            .then(artist => {
               this.artists.push(artist);
            }).catch(err => console.log('Failed to look up subscribed artist', sub.publisher));
         });
      },
      bindToController: true,
      controllerAs: 'vm', 
      access: {
         requiredLogin: true
      }
   })
   .state('parent.artist', {
      url: '/artist/:id',
      redirectTo: 'parent.artist.profile',
      abstract: true,
      views: {
         '': {
            templateUrl: '/partials/artist.html',
            controller: 'artistCtrl'
         },
         'second-menu@': {
            template: `
            <artist-nav-buttons is-admin="$resolve.isAdmin"></artist-nav-buttons>
            `
         }
      },
      access: {
         requiredLogin: false
      },
      resolve: {
         artist: function ($stateParams, ArtistAPI) {
            return ArtistAPI.findById($stateParams.id);
         },
         isAdmin: function(artist, AuthService) {
            return AuthService.isOwner(artist) || AuthService.isAdmin(artist);
         }
      }
   })
   .state('parent.artist.edit', {
      url: '/edit',
      views: {
         '': {
            template: '<artist-form artist="$resolve.artist"></artist-form>',
            controller: 'artistCtrl'
         },
         'second-menu@': {
            template: `
            <artist-admin-nav-buttons></artist-admin-nav-buttons>
            `
         }
      },
   })
   .state('parent.artist.profile', {
      url: '/profile',
      templateUrl: '/partials/artist_profile.html',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.artist.newrelease', {
      url: '/newrelease',
      templateUrl: '/partials/new_release.html',
      controller: 'newReleaseCtrl',
      controllerAs: 'vm',
      bindToController: true
   })
   .state('parent.artist.newevent', {
      url: '/newevent',
      templateUrl: '/partials/new_event.html',
      controller: 'newEventCtrl'
   })
   .state('parent.artist.newpost', {
      url: '/newpost',
      templateUrl: '/partials/new_post.html',
      controller: 'newPostCtrl'
   })
   .state('parent.artist.discography', {
      url: '/discography',
      templateUrl: '/partials/artist_discography.html',
      access: {
         requiredLogin: false
      },
      resolve: {
         releases: function (artist, ArtistReleaseFactory) {
            return ArtistReleaseFactory.query({
               id: artist.id
            }).$promise
         }
      },
      controller: 'artistReleasesCtrl',
      controllerAs: 'arc'
   })
   .state('parent.artist.events', {
      url: '/events',
      templateUrl: '/partials/artist_events.html',
      access: {
         requiredLogin: false
      },
      resolve: {
         events: function (artist, ArtistEventFactory) {
            return ArtistEventFactory.query({
               id: artist.id
            }).$promise
         }
      },
      controller: 'artistEventsCtrl',
      controllerAs: 'aec'
   })
   .state('parent.artist.blog', {
      url: '/blog',
      templateUrl: '/partials/artist_blog.html',
      resolve: {
         posts: function (artist, ArtistBlogPostFactory) {
            var Blog = ArtistBlogPostFactory(artist.id)
            console.log('BLOG', Blog)
            return Blog.query().$promise
         }
      },
      controller: 'artistBlogCtrl',
      controllerAs: 'abc',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.artist.settings', {
      url: '/settings',
      views: {
         '': {
            templateUrl: '/partials/artist_settings.html',
            controller: 'artistSettingsCtrl'
         },
         'second-menu@': {
            template: `
            <artist-admin-nav-buttons></artist-admin-nav-buttons>
            `
         }
      },
      access: {
         requiredLogin: true
      }
   })
   .state('parent.artist.social', {
      url: '/social-media',
      views: {
         '': {
            templateUrl: '/partials/artist_social_media.html',
            controller: 'artistSettingsCtrl'
         },
         'second-menu@': {
            template: `
            <artist-admin-nav-buttons></artist-admin-nav-buttons>
            `
         }
      },
      access: {
         requiredLogin: true
      }
   })
   .state('parent.artist.card', {
      url: '/card',
      template: '<tc-artist-card artist="artist"></tc-artist-card>',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.user_land', {
      url: '/userlp',
      templateUrl: '/partials/user_land.html',
      access: {
         requiredLogin: true
      }
   });
})
.factory('ArtistSubscriptions', function($http, TunecakesConfig) {
   function get(userId, page) {
      var url = TunecakesConfig.apiAddress + '/api/user/' + userId + '/follow';
      if(page) url += '?page=' + page;

      console.log("GRABBING", url)
      return $http.get(url)
      .then(res => {
         return res.data.subscriptions.filter(sub => sub.publisher_type === 'artist');
      });
   }

   function getAll(userId, cb) {
      return getTheRest(userId, 0, cb);
   }

   function getTheRest(userId, page, cb) {
      console.log("WAAAAT")
      if(typeof cb !== 'function') cb = function(){};
      console.log("oks?")

      return get(userId, page)
      .then(subs => {
         console.log("SUBS", subs)
         if(subs.length) {
            // Let the listener know we got results
            cb(subs);
         }

         // We reached the end of the line
         if(subs.length == 0) return subs;
         
         // Get the next page
         return getTheRest(userId, ++page, cb)
         .then(aggregatedSubs => {
            return subs.concat(aggregatedSubs)
         });
      })
   }

   return {
      get,
      getAll
   }
})
.factory('ArtistOwnerFactory', function (SubResourceFactory) {
   return SubResourceFactory('artist', 'owner', true);
})
.factory('ArtistAdminFactory', function (SubResourceFactory) {
   return SubResourceFactory('artist', 'admin');
})
.factory('ArtistMemberFactory', function (SubResourceFactory) {
   return SubResourceFactory('artist', 'member');
})
.factory('ArtistBlogPostFactory', function ($resource, TunecakesConfig) {
   var ArtistBlogPostFactory = function (artistId) {
      var args = [];
      args[0] = TunecakesConfig.apiAddress + '/api/artist/' + artistId + '/post/:id';
      args[1] = {
         id: '@id'
      };
      args[2] = {
         update: {
            method: 'PUT'
         }
      };

      return $resource.apply(null, args);
   };
   return ArtistBlogPostFactory;
})
.factory('ArtistFollowerFactory', function (FollowerFactory) {
   return FollowerFactory('artist');
})
.factory('ArtistFeedFactory', function (FeedFactory) {
   return FeedFactory('artist', 'everything');
})
.factory('ArtistEventFactory', function (SubResourceFactory) {
   return SubResourceFactory('artist', 'event')
})
.factory('ArtistReleaseFactory', function (SubResourceFactory) {
   return SubResourceFactory('artist', 'release')
})
.filter('Recent', function () { //most recent events filter
   return function (items) {
      var filtered = [];
      var now = new Date();

      for (var i = 0; i < items.length; i++) {
         var item = items[i];
         var itemDate = new Date(item.start_date);

         if (itemDate.getTime() <= now.getTime() && itemDate !== null) {
            filtered.push(item);
         }
      }

      return filtered;
   };
})
.filter('Upcoming', function () { //closest upcoming events filter
   return function (items) {
      var filtered = [];
      var now = new Date();

      for (var i = 0; i < items.length; i++) {
         var item = items[i];
         var itemDate = new Date(item.start_date);
         if (itemDate >= now) {
            filtered.push(item);
         }
      }

      return filtered;
   };
})
.factory('ArtistAPI', function(SearchService, TunecakesConfig, $http, $q) {
   const artistUrl = TunecakesConfig.apiAddress + '/api/artist'
   const artistSearchUrl = TunecakesConfig.apiAddress + '/api/search/artist'

   let service = {};

   service.find = (options) => {
      let {query, distance, location, sort, page} = options;
      let params = {query, sort, page};

      if (location && location.lat && location.lon) {
         params.query.mongo_location = {
            $near: {
               $geometry: { type: "Point", coordinates: [location.lon, location.lat] },
               $maxDistance: distance || 50000 //50km
            }
         }
      }

      console.log("Artist query", query, sort, page)
      return $http.get(artistUrl, {params})
      .then(res => {
         console.log("Artists found", res.data);
         return res.data;
      }).catch(err => {
         console.log("Error occurred when finding artists", err);
         throw err;
      });
   }

   service.findById = (id) => {
      return $http.get(artistUrl + '/' + id)
      .then(res => {
         console.log("Artist found by id", res.data);
         return res.data;
      }).catch(err => {
         console.log("Error occurred when finding artist " + id, err);
         throw err;
      });
   };

   service.create = (data) => {
      return $http.post(artistUrl, data)
      .then(res => {
         console.log("Successfully created an artist", res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to create an artist ", err);
         throw err;
      });
   }

   service.update = (id, data) => {
      return $http.put(artistUrl + '/' + id, data)
      .then(res => {
         console.log("Successfully updated artist", id, res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to update artist ", id, err);
         throw err;
      });
   }

   service.delete = (id) => {
      return $http.delete(artistUrl + '/' + id)
      .then(res => {
         console.log("Successfully deleted artist", id, res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to delete artist ", id, err);
         throw err;
      });
   }

   service.follow = (id, flags) => {
      return $http.post(artistUrl + '/' + id + '/follow', {flags})
      .then(res => {
         console.log("Successfully followed artist", id, res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to follow artist", id, err);
         throw err;
      });
   }

   service.unfollow = (id) => {
      return $http.delete(artistUrl + '/' + id + '/follow')
      .then(res => {
         console.log("Successfully stopped following artist", id, res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to stop following artist", id, err);
         throw err;
      });
   }

   service.followUpdate = (id, flags) => {
      return $http.put(artistUrl + '/' + id + '/follow', {flags})
      .then(res => {
         console.log("Successfully updated artist follow settings", id, res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to update artist follow settings", id, err);
         throw err;
      });
   }

   service.getReleases = (id, page) => {
      let params = {page};

      return $http.get(artistUrl + '/' + id + '/release', {params})
      .then(res => {
         console.log("Artist releases returned", res.data);
         return res.data;
      }).catch(err => {
         console.log("Error occurred when getting artist releases", err);
         throw err;
      });
   }

   service.autocomplete = name => {
      let QueryBuilder = SearchService('artistQuickSearch');
      let deferred = $q.defer();

      QueryBuilder.setFilter('artist');
      QueryBuilder.setSearchType('search');

      function handleResults(results) {
         return results.artists;
      }

      QueryBuilder.setSearchText(name);
      QueryBuilder.onResults();
      QueryBuilder.onError();
      return QueryBuilder.executeQuery().then(handleResults);
   }

   return service;
});

angular
.module('tcArtist')
.directive('artistClaimPanel', function() {
   return {
      template: `
      <div id="generated-container">
         <div id="generated-button">
            <a href="mailto:contact@tunecakes.com?subject=Profile Request - {{artist.name}} - {{artist.id}}&body=Hello%0D%0A%0D%0AI would like to request access to the Tunecakes Profile {{artist.name}} - {{artist.id}}.%0D%0A%0D%0AThank you"
              class="btn btn-default col-xs-10 col-xs-push-1">
               Is this your profile?
               </br>
               Claim it !
            </a>
         </div>
      </div>
      `
   }
})
.directive('artistForm', function() {
   return {
      scope: {
         artist: '='
      },
      templateUrl: '/partials/artist_edit.html',
      bindToController: true,
      controller: 'artistFormCtrl',
      controllerAs: 'vm'
   }
})
.directive('artistName', function () {
   return {
      scope: false,
      template: `
      <div class="col-xs-12">{{artist.name}}</div>
      `
   }
})
.directive('artistBio', function () {
   return {
      scope: false,
      templateUrl: '/partials/artist_bio.html'
   }
})
.directive('artistExternalProfiles', function () {
   return {
      scope: {
         artist: '='
      },
      template: `
      <div class="col-xs-12">
         <a ng-if="artist.official_site_url"
             target="_blank"
             href="{{artist.official_site_url}}">Artist's Site</a>
      </div>
      <div class="col-xs-12">
         <a ng-if="artist.facebook_url"
             target="_blank"
             href="{{artist.facebook_url}}">Facebook Profile</a>
      </div>
      <div class="col-xs-12">
         <a ng-if="artist.bandcamp_url"
             target="_blank"
             href="{{artist.bandcamp_url}}">Bandcamp Page</a>
      </div>
      <div class="col-xs-12">
         <a ng-if="artist.twitter_url"
             target="_blank"
             href="{{artist.twitter_url}}">Twitter Profile</a>
      </div>
      `
   }
})
.directive('artistLatestEvents', function() {
   return {
      scope: {
         artist: '=',
         isAdmin: '='
      },
      template: `
      <div class="col-xs-12"
          ng-repeat="event in artist.events | Upcoming | orderBy: 'start_date' | limitTo: 3">
         <tc-artist-event-card event="event"></tc-event-card>
      </div>
      <div class="col-xs-12 text-center"
          ng-if="artist.events.length < 1">This artist has no upcoming events</div>
      <button ng-if="isAdmin" class="col-xs-12 btn btn-primary" ui-sref="parent.artist.newevent">
          Add Event
      </button>
      `
   };
})
.directive('artistNavButtons', function() {
   return {
      scope: {
         isAdmin: '='
      },
      template: `
      <div class="artnavitem">
         <button type="button"
             class="col-xs-12 btn btn-primary"
             id="artistProfileBtn"
             analytics-on="click"
             analytics-event="View Artist Profile Tab"
             ui-sref="parent.artist.profile"
             ui-sref-active="active">Feed</button>
      </div><div class="artnavitem">
         <button type="button"
             class="col-xs-12 btn btn-primary"
             analytics-on="click"
             analytics-event="View Artist Events Tab"
             ui-sref="parent.artist.events"
             ui-sref-active="active">Events</button>
      </div><div class="artnavitem">
         <button type="button"
             class="col-xs-12 btn btn-primary"
             analytics-on="click"
             analytics-event="View Artist Profile Tab"
             ui-sref="parent.artist.discography"
             ui-sref-active="active">Discog</button>
      </div><div class="artnavitem">
         <button type="button"
             id="artistBioBtn"
             class="col-xs-12 btn btn-primary"
             analytics-on="click"
             analytics-event="View Artist Bio Tab"
             ui-sref="parent.artist.blog"
             ui-sref-active="active">Blog</button>
      </div><div class="artnavitem" ng-if="isAdmin">
         <button type="button"
             id="editArtistBtn"
             class="col-xs-12 btn btn-primary"
             ui-sref="parent.artist.edit"
             ui-sref-active="active">
            Edit
         </button>
      </div>
      `
   }
})
.directive('artistAdminNavButtons', function() {
   return {
      scope: false,
      template: `
      <div class="artnavitem">
         <button type="button"
             class="col-xs-12 btn btn-primary"
             id="artistProfileBtn"
             analytics-on="click"
             analytics-event="View Artist Profile Tab"
             ui-sref="parent.artist.profile"
             ui-sref-active="active">Profile</button>
      </div><div class="artnavitem">
         <button type="button"
             id="editArtistBtn"
             class="col-xs-12 btn btn-primary"
             ui-sref="parent.artist.edit"
             ui-sref-active="active">
            Info
         </button>
      </div><div class="artnavitem">
         <button type="button"
             class="col-xs-12 btn btn-primary"
             ui-sref="parent.artist.social"
             ui-sref-active="active">
            Social
         </button>
      </div><div class="artnavitem">
         <button type="button"
             id="artistSettingsBtn"
             class="col-xs-12 btn btn-primary"
             ui-sref="parent.artist.settings"
             ui-sref-active="active">Settings</button>
      </div>
      `
   };

})
.directive('artistNameProfile', function () {
   return {
      scope: false,
      template: `
         <artist-name tc-fill-text class="profile text-center"></artist-name>
      `
   }
})
.directive('artistMembers', function () {
   return {
      scope: false,
      template: `
      <div ng-repeat="member in artist.members" class="col-xs-12">
         <div style="display: inline-block">
            {{member.name}}
         </div>
         -
         <div style="display: inline-block">
            {{member.role}}
         </div>
      </div>
      `
   };
})
.directive('artistPicture', function () {
   return {
      scope: {
         artist: '=',
      },
      template: `
      <tc-img src="artist.image_link || 'https://i.imgur.com/lo2VFMI.png'" alt="Profile Picture"></tc-img>
      `
   }
})
.directive('artistFollowButton', function (AuthService) {
   return {
      scope: {},
      bindToController: {
         artist: '='
      },
      controller: 'artistFollowerCtrl',
      controllerAs: 'vm',
      template: `
      <!-- Follow Button -->
      <div class="row" ng-show="!vm.loggedIn">
         <a type="button"
             class="col-xs-10 btn btn-primary btn-follow"
             ui-sref="parent.login({prevState:'parent.artist.profile', prevId:vm.artist.id})">Follow</a>
      </div>
      <div class="row" ng-show="vm.loggedIn && !vm.follower">
         <button type="button"
             class="col-xs-10 btn btn-primary btn-follow"
             ng-click="vm.follow()">Follow</button>
         <button type="button"
             class="col-xs-2 btn btn-primary btn-follow"
             ng-click="vm.toggleMenu()">
            <strong ng-show="vm.menuCollapse">&#9660</strong>
            <strong ng-show="!vm.menuCollapse">&#9650</strong>
         </button>
      </div>
      <div class="row" ng-show="vm.follower">
         <button type="button"
             class="col-xs-12 btn btn-primary btn-follow"
             ng-click="vm.toggleMenu()">
            Follow Settings
            <strong ng-show="vm.menuCollapse">&nbsp;&#9660</strong>
            <strong ng-show="!vm.menuCollapse">&nbsp;&#9650</strong>
         </button>
      </div>

      <!-- Follow Settings Menu -->
      <div class="row follow-menu" uib-collapse="vm.menuCollapse">
         <div class="col-xs-6">
            <button class="btn btn-warning tc-feed-btn col-xs-10 col-xs-push-1" ng-click="vm.toggleReleaseFlag()">
               Releases
               <img height="15px" width="15px" ng-src="/img/check_mark.png" ng-if="vm.releases">
            </button>
         </div>
         <div class="col-xs-6">
            <button class="btn btn-warning tc-feed-btn col-xs-10 col-xs-push-1" ng-click="vm.toggleEventFlag()">
               Events
               <img height="15px" width="15px" ng-src="/img/check_mark.png" ng-if="vm.events">
            </button>
         </div>

         <div class="col-xs-6">
            <button class="btn btn-warning tc-feed-btn col-xs-10 col-xs-push-1" ng-click="vm.toggleBlogFlag()">
               Blog
               <img height="15px" width="15px" ng-src="/img/check_mark.png" ng-if="vm.blog">
            </button>
         </div>

         <div class="col-xs-6">
            <button class="btn btn-warning tc-feed-btn col-xs-10 col-xs-push-1" ng-click="vm.toggleOtherFlag()">
               Profile Updates
               <img height="15px" width="15px" ng-src="/img/check_mark.png" ng-if="vm.other">
            </button>
         </div>

         <button type="button"
                 ng-show="vm.follower"
                 class="col-xs-4 col-xs-push-4 tc-feed-btn btn btn-danger"
                 ng-click="vm.unfollow()">
            Unfollow
         </button>
      </div>
      `
   }
})
.directive('simpleArtistFollowButton', function () {
   return {
      scope: false,
      template: `
      <button type="button"
          class="btn btn-primary col-xs-12 art-card art-card-follow"
          ng-click="vm.toggleFollower()">
         Follow
         <div class="tc-btn-disabled" ng-if="!vm.isInit || vm.isFollowing || !vm.AuthService.isLoggedIn()"></div>
         <img class="tc-btn-selected" src="img/check_mark.png"  ng-if="vm.isFollowing">
      </button>
      `
   }
})
.directive('tcArtistCard', function () {
   return {
      templateUrl: '/partials/artist_card.html',
      scope: {
         artist: '='
      },
      restrict: 'E',
      controller: 'artistSimpleCtrl',
      controllerAs: 'vm',
      bindToController: true
   };
})
.directive('tcArtistFormSimple', function () {
   return {
      templateUrl: '/partials/artist_form_simple.html',
      scope: false,
      restrict: 'E'
   };
})
.directive('artistLocation', function () {
   return {
      scope: false,
      template: `
      <span>{{artist.city}}</span>
      <span ng-if="artist.city && (artist.state || artist.country)">,</span>
      <span>{{artist.state}}</span>
      <span ng-if="artist.state && artist.country">,</span>
      <span>{{artist.country}}</span>
      `
   }
});

angular
.module('tcUser', [])
.factory('UserAPI', function(TunecakesConfig, AuthService, $http) {
   const userUrl = TunecakesConfig.apiAddress + '/api/user';

   let service = {};
   
   service.getSubscribedArtists = () => {
      let user = AuthService.getUser();
      return $http.get(userUrl + '/' + user.id + '/follow')
      .then(res => {
         console.log("Successfully got all subscribed artists for user", user.id, res.data);
         return res.data;
      }).catch(err => {
         console.log("Failed to get subscribed artists for user", user.id, err);
         throw err;
      });
   }

   return service;
});

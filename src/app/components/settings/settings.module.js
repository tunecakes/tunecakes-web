angular
.module('tcSettings', [])
.config(function($stateProvider) {
   $stateProvider
   .state('parent.settings', {
      url: '/settings',
      templateUrl: '/partials/settings.html',
      controller: 'settingsCtrl',
      access: {
         requiredLogin: true
      }
   });
});

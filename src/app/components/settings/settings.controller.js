angular
.module('tcSettings')
.controller('settingsCtrl', function ($scope, $state, user, UserFactory, MessageService) {
   $scope.user = user;
   $scope.UserFactory = UserFactory;

   $scope.submit = function () {
      $scope.user.$update(function (res) {
         MessageService.successAfterRedirect($state.reload(),
            'Settings updated succesfully!');
      }, function (fail) {
         MessageService.parseHttpError(fail);
      });
   };

   $scope.deleteAccount = function () {
      var confirmDelete = confirm(
         'Are you sure you want to delete your account?'
      );
      if (confirmDelete) {
         $scope.user.$remove(function (res) {
            $state.go('parent.logout');
         });
      }
   }
});

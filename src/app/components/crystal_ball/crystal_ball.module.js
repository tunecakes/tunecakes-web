angular
.module('tcCrystalBall', [])
.config(function($stateProvider){
   $stateProvider
   .state('parent.crystal_ball', {
      url:'/crystal_ball',
      template: '<crystal-ball></crystal-ball>'
   })
})
.directive('crystalBall', function(){
   return {
      templateUrl:'/partials/crystal_ball.html',
      controller:'tcCrystalBallCtrl',
      controllerAs:'vm',
      bindToController: true
   }
})
.controller('tcCrystalBallCtrl', function($stateParams, NearbyEvents) {
   const artistsPerPage = 10;
   this.activeArtists = [];
   this.filteredArtists = [];
   this.page = 0;
   this.totalPages = 0;

   this.artistIds = [];
   this.artistMap = {};

   this.tags = [];
   this.activeTags = [];
   this.tagsMap = {};

   let nearbyEvents = new NearbyEvents();
   let processEvents = events => {
      events.forEach(e => {
         e.linked_artists.forEach(a => {
            if(!this.artistMap[a.id]) {
               this.artistMap[a.id] = {
                  artist: a,
                  events: []
               };

               this.artistIds.push(a.id);
               this.addArtistTags(a.tags);
            }

            this.artistMap[a.id].events.push(e);
         });
      });

      return events;
   };

   this.nextPage = () => {
      if(this.page >= this.totalPages - 1) return;

      this.page++;
      this.generateActiveArtists();
   }

   this.prevPage = () => {
      if(this.page <= 0) return;

      this.page--;
      this.generateActiveArtists();
   }

   // Set page to a specific number
   this.setPage = (pageNum) => {
      if(pageNum >= this.totalPages) {
        this.page = this.totalPages;
        this.generateActiveArtists();
        return;
      }

      if(pageNum <= 0){
        this.page = 0;
        this.generateActiveArtists();
        return;
      }

      this.page = pageNum;
      this.generateActiveArtists();
   }

   this.activeArtistOrder = aid => {
      let events = this.artistMap[aid].events
      let earliestDate = events[0].start_date;

      events.forEach(e => {
         if(e.start_date < earliestDate) {
            earliestDate = e.start_date;
         }
      });

      return earliestDate;
   }

   this.generateActiveArtists = () => {
      let startIdx = this.page * artistsPerPage;
      let endIdx = startIdx + artistsPerPage;
      this.activeArtists = this.filteredArtists.slice(startIdx, endIdx);
   }

   this.generateFilteredArtists = () => {
      if(!this.activeTags.length) {
         this.filteredArtists = this.artistIds;
         return;
      }

      this.filteredArtists = [];
      this.artistIds.forEach(aid => {
         let artist = this.artistMap[aid].artist;

         for(let i = 0; i < artist.tags.length; i++) {
            let tag = artist.tags[i];

            if(this.activeTags.indexOf(tag) < 0) continue;

            this.filteredArtists.push(aid);
            break;
         }
      });
   }

   this.calculatePages = () => {
      this.totalPages = Math.ceil(this.filteredArtists.length/artistsPerPage);
   }

   this.refreshArtistsView = () => {
      this.page = 0;
      this.generateFilteredArtists();
      this.calculatePages();
      this.generateActiveArtists();
   }

   this.filterByTag = aid => {
      let artist = this.artistMap[aid].artist;
      let hasAllActiveTags = true;

      if(this.activeTags.length === 0) return true;

      artist.tags.forEach(t => {
         if(this.activeTags.indexOf(t) < 0) {
            hasAllActiveTags = false;
         }
      });

      return hasAllActiveTags;
   }

   this.addArtistTags = tags => {
      if(!tags || !tags.length) return;

      tags.forEach(t => {
         if(!this.tagsMap[t]) {
            this.tagsMap[t] = {
               count: 0
            }
            this.tags.push(t);
         }

         this.tagsMap[t].count++;
      });
   }

   this.deactivateTagFilters = () => {
      this.activeTags.splice(0, this.activeTags.length);
      this.refreshArtistsView();
   }

   this.toggleTagFilter = (tag) => {
      let tagIndex = this.activeTags.indexOf(tag);

      if(tagIndex < 0) {
         this.activeTags.push(tag);
      } else {
         this.activeTags.splice(tagIndex, 1);
      }

      this.refreshArtistsView();
   }

   this.getTagCount = (tag) => {
      return this.tagsMap[tag].count;
   }

   this.eventAPICalls = 1;
   this.getMoreEvents = (events) => {
      if(!events.length || this.eventAPICalls > 5) {
         this.refreshArtistsView();
         return;
      }

      this.eventAPICalls++;
      nearbyEvents.more(processEvents)
      .then(this.getMoreEvents);
   }

   nearbyEvents.get()
   .then(processEvents)
   .then(this.getMoreEvents);
});

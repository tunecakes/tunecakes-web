angular
.module('tcEventCalendar', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.event_calendar', {
      url: '/event_calendar',
      templateUrl: '/partials/event.html',
      controller: 'eventCtrl',
   })
})
.directive('tcEventCalendar', function() {
   return {
      scope: {
         location: '='
      },
      templateUrl: '/partials/calendar.html',
      controller: 'tcEventCalendarCtrl',
      controllerAs: 'vm',
      bindToController: true
   }
})
.factory('EventCalendarService', function(uiCalendarConfig) {
   let eventFactory = (title, start, id) => {
      return {
         events: [{
            title,
            start,
            id
         }],
         color: '#A285BD',
         textColor: '#FFF'
      };
   };

   let convertEventsToEventSource = (events) => {
      events = events.map(e => {
         let title;

         if (!e.artists.length) {
            title = 'TBA'
         } else {
            // First 3 artist names only, joined by a new line
            title = e.artists.slice(0, 3).map(a => a.name).join('\n');
         }

         return {
            title: title,
            start: e.start_date,
            id: e.id,
            rawEvent: e
         }
      });

      return {
         events,
         color: '#A285BD',
         textColor: '#FFF'
      };
   }

   let refreshCalendar = () => {
      uiCalendarConfig.calendars.tcCalendar.fullCalendar('rerenderEvents');
   }

   return {
      convertEventsToEventSource,
      refreshCalendar
   };
})
.controller('tcEventCalendarCtrl', function($state, $timeout, $window, EventCalendarService, EventAPI) {
   let activeLocation;
   let defaultView = 'month';
   let today = new Date();
   today.setHours(0, 0, 0, 0);

   this.tags = [];
   this.activeTags = [];
   this.tagsMap = {};

   this.defaultLocation = {
      lat: 35.7796,
      lon: -78.6382
   }

   this.eventSources = [];
   this.place = {};

   if($window.innerWidth < 768) {
      defaultView = 'listYear';
   }

   this.config = {
      themeSystem: 'bootstrap3',
      timezone: 'local',
      scrollTime: "12:00:00",
      defaultTimedEventDuration: "1:00:00",
      defaultView: defaultView,
      displayEventTime: false,
      editable: true,
      height: 700,
      header: {
         left: 'prev,next',
         center: 'title',
         right: 'month,listYear',
      },
      eventClick: calEvent => {
         $state.go('parent.event.main', {id: calEvent.id})
      },
      eventRender: calEvent => {
         if(!this.activeTags.length) return;

         return calEvent.rawEvent.artists.some(a => {
            if(!a.tags) return false;
            return a.tags.some(t => {
               if(this.activeTags.includes(t)) return true;
               return false;
            });
         });
      },
      eventLimit: true
   };

   let addEvents = (events) => {
      let sources = EventCalendarService.convertEventsToEventSource(events)
      this.eventSources.push(sources);
      EventCalendarService.refreshCalendar();

      // Collect the tags for these events
      events.forEach(e => {
         let eventTags = [];
         e.linked_artists.forEach(a => {
            a.tags.forEach(t => {
               if(eventTags.includes(t)) return;
               eventTags.push(t);
            })
         });

         eventTags.forEach(t => {
            if(!this.tagsMap[t]) {
               this.tagsMap[t] = {
                  count: 0
               }
               this.tags.push(t);
            }

            this.tagsMap[t].count++;
         });
      });

      return events
   };

   let getEvents = (date, page) => {
      let loc = activeLocation;
      page = page || 0;

      if(!loc || !loc.lat || !loc.lon) {
         loc = this.defaultLocation;
      }

      EventAPI.findAfterDate(date, {location: loc, page: page})
      .then(addEvents)
      .then(events => {
         if(!events.length) return;
         if(page >= 5) return;

         getEvents(date, ++page);
      })
   }
   
   let clearEvents = () => {
      this.eventSources.splice(0, this.eventSources.length - 1)
   };

   this.removeTagFilters = () => {
      this.activeTags.splice(0, this.activeTags.length);
   }

   this.deactivateTagFilters = () => {
      this.removeTagFilters();
      EventCalendarService.refreshCalendar();
   }

   this.toggleTagFilter = (tag) => {
      let tagIndex = this.activeTags.indexOf(tag);

      if(tagIndex < 0) {
         this.activeTags.push(tag);
      } else {
         this.activeTags.splice(tagIndex, 1);
      }

      EventCalendarService.refreshCalendar();
   }

   this.getTagCount = (tag) => {
      return this.tagsMap[tag].count;
   }

   this.onLocationUpdate = (location) => {
      clearEvents();
      this.removeTagFilters();
      this.tags = [];
      this.tagsMap = {};
      activeLocation = location;
      getEvents(today);
   }

   this.$onInit = () => {
      activeLocation = this.location;
      this.place.location = activeLocation;
      getEvents(today);
   };
});

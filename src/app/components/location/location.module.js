angular
.module('tcLocation', [])
.factory('GoogleGeo', function($q) {
   var geocoder = new google.maps.Geocoder;
   
   function reverseGeocode(lat, lng) {
      var latlng = {lat, lng};
      var d = $q.defer();

      geocoder.geocode({location: latlng}, function(results, status) {
         if (status === 'OK') {
            if (results[0]) {
               d.resolve(results[0])
            } else {
               d.reject(new Error("No reverse geocode results for location", latlng))
            }
         } else {
            d.reject('Geocoder failed due to: ' + status);
         }
      });

      return d.promise;
   }

   function parseAddressComponents(addressComponents) {
      var address, streetNumber, road, city, state, country, zip;

      for (let i = 0; i < addressComponents.length; i++) {
         let addressType,
            component = addressComponents[i],
            name = component.short_name || component.long_name,
            types = component.types;

         types = types.filter(t => t !== 'political')

         if(!types || !types.length || types.length === 0) {
            console.log("Unable to handle this address component", component);
            continue;
         } else if (types.length === 1) {
            addressType = types[0];
         } else {
            addressType = types[0];
            console.log('Defaulting to first type listed in array for this address component', component);
         }

         switch(addressType) {
         case "street_number":
            streetNumber = name;
            break;
         case "route":
            road = name;
            break;
         case "locality":
            city = name;
            break;
         case "administrative_area_level_1":
            state = name;
            break;
         case "country":
            country = name;
            break;
         case "postal_code":
            zip = name;
            break;
         }
      }

      if(streetNumber && streetNumber !== '') {
         address = streetNumber + ' ' + road;
      } else {
         address = road;
      }

      return {
         address,
         city,
         state,
         country,
         zip
      }
   }

   function lookupAddress(lat, lng) {
      return reverseGeocode(lat, lng).then(r => {
         return parseAddressComponents(r.address_components);
      });
   }

   function searchByString(address, regionOnly) {
      var d = $q.defer();

      let searchParams = {address};
      let options = {};
      if (regionOnly) {
         searchParams.region = "(region)";
      }

      geocoder.geocode(searchParams, function(results, status) {
         if (status === 'OK') {
            if (results.length) {
               d.resolve(results)
            } else {
               d.reject(new Error("No geocode results for address", address));
            }
         } else {
            d.reject('Geocoder failed to get cordinates due to: ' + status, address);
         }
      });

      return d.promise;

   }

   return {
      reverseGeocode,
      parseAddressComponents,
      lookupAddress,
      searchByString
   };
})
.directive('googlePlaceAutocomplete', function (GoogleGeo) {
   return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
         'ngModel': '=',
         'updateLocation': '&',
         'defaultLocation': '<',
         'placeholder': '@',
         'regionOnly': '<'
      },
      template: `
      <input type="text"
          class="form-control input-text"
          ng-model="location"
          placeholder="{{ placeholder || 'Enter a Location'}}"
          autocomplete="off">`,
      link: function (scope, iElement, iAttrs, ngModel) {
         var el = angular.element(iElement.find('input'));
         var options = {};

         if (scope.regionOnly) {
            options.types = ['(regions)'];
         }

         var autocomplete = new google.maps.places.Autocomplete(el[0], options);

         // When the user selects an address from the dropdown, populate the address
         // fields in the form.
         autocomplete.addListener('place_changed', fillInAddress);

         function addComponent(cmp) {
            if(!cmp || cmp === '') return;

            if(!scope.location) scope.location = '';
            else if(scope.location !== '') scope.location += ', ';

            scope.location += cmp;
         }

         scope.location = '';
         setLocationView();

         var startingLocation = scope.ngModel.location || scope.defaultLocation;
         if (startingLocation) {
            GoogleGeo.lookupAddress(startingLocation.lat, startingLocation.lon)
            .then(r => {
               addComponent(r.city);
               addComponent(r.state);
               addComponent(r.country);
            })
         }

         function setLocationView() {
            scope.location = '';

            if(scope.ngModel.venue_name != scope.ngModel.city &&
               scope.ngModel.venue_name != scope.ngModel.address) {
               addComponent(scope.ngModel.venue_name)
            }
            addComponent(scope.ngModel.address);
            addComponent(scope.ngModel.city);
            addComponent(scope.ngModel.state);
            addComponent(scope.ngModel.country);
         }

         function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            console.log('Found place', place);
            var loc = {
               lat: place.geometry.location.lat(),
               lon: place.geometry.location.lng()
            };
            if (scope.updateLocation) scope.updateLocation({
               location: loc
            });

            if(scope.ngModel.location) {
               scope.ngModel.location = loc;
            }

            // map google response to our field names
            var parsed = GoogleGeo.parseAddressComponents(place.address_components);
            console.log('Parsed place', parsed);
            scope.ngModel.address = parsed.address || "";
            scope.ngModel.city = parsed.city || "";
            scope.ngModel.state = parsed.state || "";
            scope.ngModel.country = parsed.country || "";
            scope.ngModel.zip = parsed.zip || "";

            scope.ngModel.venue_name = place.name || "";
            setLocationView();

            scope.$apply();
         }
      }
   };
}
);

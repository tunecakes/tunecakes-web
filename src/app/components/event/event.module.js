angular
.module('tcEvent', [])
.config(function ($stateProvider) {
   $stateProvider
   .state('parent.event', {
      url: '/event/:id',
      redirectTo: 'parent.event.main',
      abstract: true,
      views: {
         '': {
            templateUrl: '/partials/event.html',
            controller: 'eventCtrl'
         },
         'second-menu@': {
            template: `
            <div class="artnavitem">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   ui-sref="parent.event.main"
                   ui-sref-active="active">
                  Info
               </button>
            </div><div class="artnavitem" ng-show="$resolve.isAdmin">
               <button type="button"
                   class="col-xs-12 btn btn-primary"
                   ui-sref="parent.event.edit"
                   ui-sref-active="active">
                  Edit
               </button>
            </div><div class="artnavitem" ng-show="$resolve.isAdmin">
               <button type="button"
                   class="col-xs-12 btn btn-danger"
                   ng-click="delete()">
                  Delete
               </button>
            </div>
            `,
            controller: 'eventCtrl'
         }
      },
      resolve: {
         event: function ($stateParams, EventFactory) {
            return EventFactory.get({
               id: $stateParams.id
            }).$promise;
         },
         isAdmin: function(event, AuthService) {
            return AuthService.isOwner(event) || AuthService.isAdmin(event);
         },
         eventFollower: function ($stateParams, AuthService, EventFollowerFactory, resolved) {
            // only fetch if user is logged in
            return null;
            //AuthService.isLoggedIn() ? EventFollowerFactory.get({
            //    id: $stateParams.id
            //}).$promise : null;
         }
      },
      access: {
         requiredLogin: false
      }
   })
   .state('parent.event.main', {
      url: '/info',
      templateUrl: '/partials/event_main.html',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.event.calendar', {
      url: '/calendar',
      template: '<tc-event-calendar events="[$resolve.event]"></tc-event-calendar>',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.event.edit', {
      url: '/edit',
      templateUrl: '/partials/event_edit.html',
      access: {
         requiredLogin: true
      }
   })
   .state('parent.new_event', {
      url: '/new_event',
      templateUrl: '/partials/new_event.html',
      controller: 'newEventCtrl',
      access: {
         requiredLogin: true
      }
   });
})
.factory('EventAPI', function(TunecakesConfig, $http) {
   const eventUrl = TunecakesConfig.apiAddress + '/api/event'
   const eventSearchUrl = TunecakesConfig.apiAddress + '/api/search/event'

   let service = {};

   service.findAfterDate = (date, options) => {
      options = options || {};
      options.sort = "start_date";
      options.query = {
         start_date: {
            $gte: date
         }
      };

      return service.find(options);
   }

   service.find = (options) => {
      let {
         distance,
         location,
         page,
         query,
         sort,
         tags
      } = options;

      query = query || {};

      if (location && location.lat && location.lon) {
         query.mongo_location = {
            $near: {
               $geometry: { type: "Point", coordinates: [location.lon, location.lat] },
               $maxDistance: distance || 50000 //50km
            }
         }
      }

      if(tags) {
         query.tags = {
            $all: ['asdf']
         }
      }
      
      console.log("Event query", query, sort, page)

      let params = {query, sort, page};
      return $http.get(eventUrl, {params})
      .then(res => {
         console.log("Events found", res.data);
         return res.data;
      }).catch(err => {
         console.log("Error occurred when finding events", err);
         throw err;
      });
   }

   return service;
})
.factory('NearbyEvents', function(EventAPI, AuthService) {
   function Factory(date, location) {
      this.init(date, location);
   }

   Factory.prototype.init = function (date, location) {
      if(!date) {
         this.date = new Date();
         this.date.setHours(0, 0, 0, 0);
      } else {
         this.date = date;
      }

      if(!location) {
         let user = AuthService.getUser();
         if(!user || !user.location) {
            // This is Raleigh, NC
            this.location = {
               lat: 35.7796,
               lon: -78.6382
            };
         } else {
            this.location = user.location;
         }
      } else {
         this.location = location;
      }

      this.page = 0;
   }

   Factory.prototype.get = function() {
      return EventAPI.findAfterDate(this.date, {
         location: this.location,
         page: this.page
      });
   }

   Factory.prototype.more = function() {
      this.page++;
      return this.get();
   }

   return Factory;
})
.factory('EventFactory', function (ResourceFactory) {
   return ResourceFactory('/event/:id', true, true);
})
.factory('EventOwnerFactory', function (SubResourceFactory) {
   return SubResourceFactory('event', 'owner', true);
})
.factory('EventAdminFactory', function (SubResourceFactory) {
   return SubResourceFactory('event', 'admin');
})
.factory('EventArtistFactory', function (SubResourceFactory) {
   return SubResourceFactory('event', 'artist');
})
.factory('EventFollowerFactory', function (FollowerFactory) {
   return FollowerFactory('event');
});

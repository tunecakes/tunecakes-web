angular
.module('tcEvent')
.controller('eventCtrl', function ($scope, $state, $q, $log, event, eventFollower,
   AggregatedFeedFactory, ImgurUpload, AuthService, MessageService) {
   $scope.isAdmin = false;
   $scope.submitted = false;
   $scope.AuthService = AuthService;
   $scope.event = event;
   $scope.eventFollower = eventFollower;
   $scope.feed = AggregatedFeedFactory(event.id, 'event', 'everything');

   $scope.images = {
      main: false,
      other: []
   };

   function getImageLink() {
      if (!$scope.images.main) return $q.resolve($scope.event.image_link);

      MessageService.success('Saving image...');

      return ImgurUpload.uploadImage($scope.images.main)
         .then(function (res) {
            $log.log('imgur upload success', res)
            return res.data.data.link;
         });
   }

   $scope.save = function () {
      getImageLink()
         .catch(function (err) {
            MessageService.error('Failed to upload image. ' + err.data.data.error);
            throw err;
         })
         .then(function (link) {
            $scope.event.image_link = link;
            return $scope.event.$update();
         }).then(function () {
            AuthService.updateUser();
            $state.go('parent.event.main', {id: $scope.event.id});
         });
   };

   $scope.delete = function () {
      if (!confirm('Are you sure you want to delete this event?')) return;
      $scope.event.$remove();
      $state.go('parent.feed.events');
   };

   if (AuthService.isOwner(event) || AuthService.isAdmin(event)) {
      $scope.isAdmin = true;
   }
})
.controller('eventFollowCtrl', function ($scope, EventFollowerFactory, $analytics) {
   var eventFollower = $scope.$parent.eventFollower;
   var event = $scope.$parent.event;
   /*
    * Follow Module
    */

   if (eventFollower && eventFollower.flags) {
      $scope.isFollowing = true;
      $scope.follow = {
         announcements: eventFollower.flags.indexOf('announcements') > -1,
         reminder: eventFollower.flags.indexOf('reminder') > -1,
         other: eventFollower.flags.indexOf('other') > -1
      };
   } else {
      $scope.isFollowing = false;
      $scope.follow = {
         announcements: false,
         reminder: true,
         other: false
      };
   }

   $scope.followCollapse = true;
   $scope.toggleFollowCollapse = function () {
      $scope.followCollapse = !$scope.followCollapse;
   };
   $scope.getFollowFlags = function () {
      var followFlags = [];

      if ($scope.follow.announcements) followFlags.push('announcements');
      if ($scope.follow.reminder) followFlags.push('reminder');
      if ($scope.follow.other) followFlags.push('other');

      return followFlags;
   };
   $scope.addFollower = function () {
      $scope.isFollowing = true;
      // might be better if we can add this to the resource factory
      $analytics.eventTrack("Follow Event", {
         event: event.id,
         flags: $scope.getFollowFlags()
      });

      EventFollowerFactory.follow({
         id: event.id
      }, {
         flags: $scope.getFollowFlags()
      }, function (follower) {
         eventFollower = follower;
      });
      $scope.followCollapse = true;
   };
   $scope.removeFollower = function () {
      $scope.isFollowing = false;
      eventFollower.$unfollow({
         id: event.id
      });
      $scope.followCollapse = true;
   };
   $scope.updateFollower = function () {
      eventFollower.flags = $scope.getFollowFlags();
      eventFollower.$updateFollower({
         id: event.id
      });
      $scope.followCollapse = true;
   };
})
.controller('newEventCtrl', function ($scope, $log, $q, AuthService, EventFactory,
   $state,
   MessageService, ImgurUpload) {
   $scope.event = {
      artists: [{
         id: $scope.artist.id
      }]
   };

   $scope.images = {
      main: false,
      other: []
   };

   $scope.EventFactory = EventFactory;

   function getImgurLink() {
      if (!$scope.images.main) return $q.resolve();

      MessageService.success('Saving image...');

      return ImgurUpload.uploadImage($scope.images.main)
         .then(function (res) {
            $log.log('imgur upload success', res)
            return res.data.data.link;
         }).catch(function (err) {
            $log.error('imgur upload fail', err)
            MessageService.success('Failed to save image, try again later');
            return;
         });
   }

   $scope.open = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
   };

   $scope.onChange = function (start) {
      if ($scope.event.end_date && start < $scope.event.end_date) return;

      $scope.event.end_date = new Date(start.getTime());
      $scope.event.end_date.setHours($scope.event.end_date.getHours() + 1);
   };

   $scope.submit = function () {
      $log.log("Processing event..");

      var _event = new EventFactory($scope.event)

      _event.$save()
         .then(getImgurLink)
         .then(function (link) {
            var done = $q.resolve();

            if (link) {
               _event.image_link = link;
               done = _event.$update();
            }

            done.then(function () {
               MessageService.successAfterRedirect(
                  $state.go('parent.event.main', {
                     id: _event.id
                  }),
                  'Event successfuly created!'
               );
            })
         })
         .catch(MessageService.parseHttpError);
   };
});

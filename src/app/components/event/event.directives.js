angular
.module('tcEvent')
.directive('tcEventFormSimple', function () {
   return {
      templateUrl: '/partials/event_form_simple.html',
      scope: false,
      restrict: 'E'
   };
})
.directive('tcEventCard', function () {
   return {
      templateUrl: '/partials/event_card.html',
      scope: {
         event: '&'
      },
      restrict: 'E',
      link: function (scope, ele, attrs) {
         scope.object = scope.event();
         scope.genreTags = [];
         scope.addGenres = function (tags) {
            for (var tag in tags) {
               if (scope.genreTags.indexOf(tags[tag]) < 0) scope.genreTags.push(
                  tags[tag]);
            }
         };
      }
   };
})
.directive('tcArtistEventCard', function () {
   return {
      templateUrl: '/partials/artist_event_card.html',
      scope: {
         event: '&'
      },
      restrict: 'E',
      link: function (scope, ele, attrs) {
         scope.object = scope.event();
         scope.genreTags = [];
         scope.addGenres = function (tags) {
            for (var tag in tags) {
               if (scope.genreTags.indexOf(tags[tag]) < 0) scope.genreTags.push(
                  tags[tag]);
            }
         };
      }
   };
})
.directive('tcEventView', function (EventFactory) {
   return {
      restrict: 'E',
      scope: {
         id: '@',
         data: '=?'
      },
      link: function (scope, element, attrs, controllers) {
         if (attrs.data) {
            scope.event = attrs.data;
         } else {
            scope.event = EventFactory.get({
               'id': attrs.id
            });
         }
      },
      templateUrl: '/partials/event.html'
   };
})
.directive('tcEventNotifiCard', function () {
   return {
      priority: 1001,
      templateUrl: '/partials/event_notification.html',
      scope: {
         data: '&'
      },
      link: function (scope) {
         var activity = scope.data();
         var object = activity.object;
         var objectType = activity.objectType;

         scope.event = activity.actor;
         scope.text = activity.verb;

         if(objectType === 'null') ;
         else if(objectType === 'string')
            scope.text += ' ' + object;
         else scope.text += ' ' + object.name;

      }
   };
});

angular
.module('tunecakes')
.config(function ($urlRouterProvider, $stateProvider) {
   $stateProvider
   .state('parent', {
      abstract: true,
      templateUrl: '/partials/parent.html',
      resolve: {
         resolved: function (AuthService, $q) {
            // we need to make sure that this is never rejected or every child state will fail
            var deferred = $q.defer();
            AuthService.resolve()
            .then(undefined, angular.identity)
            .then(() => {
               return deferred.resolve();
            });
            return deferred.promise;
         },
         user: function(resolved, AuthService) {
            return AuthService.getUser();
         }
      }
   })
   .state('parent.connect_artist_accounts', {
      url: '/connect_artist_accounts',
      templateUrl: '/partials/connect_artist.html',
      controller: 'artistAccountsCtrl',
      access: {
         requiredLogin: false
      }
   })
   .state('parent.connect_user_accounts', {
      url: '/connect_user_accounts',
      templateUrl: '/partials/connect_user_accounts.html',
      controller: 'userAccountsCtrl',
      access: {
         requiredLogin: false
      }
   });

   $urlRouterProvider.otherwise(function ($injector) {
      var $state = $injector.get('$state');
      $state.go('parent.main');
   });
});

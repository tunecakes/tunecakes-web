angular
.module('tcResources', [])
.factory('ResourceFactory', function ($resource, TunecakesConfig) {
   var ResourceFactory = function (path, includeId, includeUpdate) {
      var baseUrl = TunecakesConfig.apiAddress + '/api'
      var args = [ baseUrl + path, {}, {}];
      if (includeId) {
         args[1] = {
            id: '@id'
         };
      }

      if (includeUpdate) {
         args[2] = {
            'update': {
               method: 'PUT'
            },
            'validate': {
               method: 'POST',
               url: baseUrl + '/validate' + path
            }
         };
      }

      return $resource.apply(null, args);
   };
   return ResourceFactory;
})
.factory('SubResourceFactory', function ($resource, TunecakesConfig) {
   var SubResourceFactory = function (path, subPath, includeUpdate) {
      var args = [];
      args[0] = TunecakesConfig.apiAddress + '/api/' + path + '/:id/' + subPath + '/:subId';
      args[1] = {id: '@id', subId: '@subId'};
      if(includeUpdate) args[2] = {update: {method: 'PUT'}};

      return $resource.apply(null, args);
   };
   return SubResourceFactory;
})  
.factory('FollowerFactory', function($resource, TunecakesConfig) {

   var FollowerFactory = function(type) {
      var args = [TunecakesConfig.apiAddress + '/api/' + type + '/:id/follow', {}, {}];

      args[1] = {
         id: '@id'
      };

      args[2].follow = {
         method: 'POST'
      };

      args[2].unfollow = {
         method: 'DELETE'
      };

      args[2].updateFollower = {
         method: 'PUT'
      };

      return $resource.apply(null, args);
   };

   return FollowerFactory;
})
.factory('UserFactory', function (ResourceFactory) {
   return ResourceFactory('/user/:id', true, true);
});

angular
.module('tcDirectives', [])
.filter('asMinutes', function () {
   function asMinutes(input, minutePadding) {
      if (!input) return;

      var totalSecs = input / 1000;
      var mins = Math.floor(totalSecs / 60);
      var secs = Math.trunc(totalSecs % 60);

      function addPadding(unit) {
         return unit <= 9 ? "0" + unit : unit;
      }

      if (minutePadding) {
         mins = addPadding(mins)
      }

      return mins + ':' + addPadding(secs);
   }
   return asMinutes;
})
.directive('panel', function() {
   return {
      transclude: true,
      template: `
      <div class="row">
         <div class="panel panel-default">
            <div class="panel-body" ng-transclude></div>
         </div>
      </div>
      `
   }
})
.directive('headerPanel', function() {
   return {
      transclude: {
         'heading': 'panelHeading',
         'body': 'panelBody'
      },
      template: `
      <div class="row">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title" ng-transclude="heading"></h3>
            </div>
            <div class="panel-body" ng-transclude="body"></div>
         </div>
      </div>
      `
   }
})
.filter('toMilliseconds', function () {
   function toMilliseconds(input) {
      var parts = input.split(':');
      var mins = parseInt(parts[0]);
      var secs = parseInt(parts[1]) || 0;
      return (60 * mins + secs) * 1000;
   }
   return toMilliseconds;
})
.directive('tcFillText', function($window, $timeout) {
   return {
      restrict: 'A',
      link: function (scope, element) {
         // This will wrap on spaces and long words
         element.css({'word-wrap': 'break-word'});
         // This lets us compare against scrollheight
         element.css({'overflow': 'hidden'});

         function isTextFit() {
            return element[0].scrollHeight <= element[0].offsetHeight &&
                   getFontSize() <= element[0].offsetHeight;
         }

         function getFontSize() {
            return parseInt(element.css('font-size').slice(0, -2))
         }

         function growText() {
            element.css('font-size', getFontSize() + 1 + 'px');
            if (isTextFit()) growText();
            else shrinkText();
         }

         function shrinkText() {
            element.css('font-size', getFontSize() - 1 + 'px');
            if (isTextFit()) return;
            else shrinkText();
         }


         function resizeText() {
            if(element.css('height') === '' && element.css('maxHeight') === '') {
               throw new Error('tc-fill-text directive requires either "height" or "max-height" CSS to be set');
            }

            // No text in element
            if(element.text().trim() === '') return;
            if (getFontSize() < 1) return;

            if (isTextFit()) growText();
            else shrinkText();
         }

         

         var evntRunning = false;
         element.on("blur keyup change", function () {
            if(evntRunning) return

            evntRunning = true;
            resizeText();
            evntRunning = false;
         });

         // Run this after element is init'd
         $timeout(resizeText, 0)
         // Run this when window updates
         angular.element($window).bind('resize', resizeText);
      }
   }
})
.directive('tcImg', function () {
   return {
      restrict: 'E',
      scope: {
         src: '='
      },
      template: `
      <div 
         class="tc-img"
         ng-style="{'background-image': 'url(' + src || '/img/no_image.jpg' + ')'}">
      </div>
      `,
      replace: true
   };
})
.directive('tcRequiredInfo', function() {
   return {
      restrict: 'E',
      template: `
         <span style="color: red; font-weight:800; font-style: italic">
            * Indicates a required field
         </span>
      `
   };
})
.directive('tcRequired', function() {
   return {
      restrict: 'E',
      template: `
         <span style="color: red; font-weight:800">*</span>
      `
   };
})
.directive('tcDateInput', function () {
   return {
      restrict: 'E',
      require: 'ngModel',
      template: `
         <div class="dropdown row"
             is-open="isOpen"
             uib-dropdown>
            <a href tabindex="-1">
               <div class="input-group col-xs-12">
                  <button class="btn btn-primary col-xs-12"
                      uib-dropdown-toggle>
                     {{ (model || 'Select a date') | date: 'short' }}
                     <i class="glyphicon glyphicon-calendar"></i>
                  </span>
               </div>
            </a>
            <ul class="dropdown-menu"
                role="menu">
               <datetimepicker ng-model="model"
                   data-datetimepicker-config="config"
                   data-on-set-time="onTimeSet(newDate, oldDate)"
                   ng-model-options="{ updateOn: 'default' }">
               </datetimepicker>
            </ul>
         </div>
      `,
      scope: {
         'model': '=ngModel',
         'name': '@',
         'type': '@',
         'onChange': '&'
      },
      link: function (scope) {
         var type = scope.type || 'datetime';

         if (type === 'datetime') {
            scope.format = "MM/DD/YYYY hh:mm A";
            scope.placeholder = "MM/DD/YYYY HH:MM AM";
            scope.mask = "19/39/2999 29:69 pm";
            scope.config = {
               minuteStep: 15,
               minView: 'minute'
            }
         } else if (type === 'date') {
            scope.format = "YYYY-MM-DD";
            scope.placeholder = "YYYY-MM-DD";
            scope.mask = "2999-39-19";
            scope.config = {
               minView: 'day',
               startView: 'day'
            }
         } else if (type === 'month') {
            scope.format = "YYYY-MM";
            scope.placeholder = "YYYY-MM";
            scope.mask = "2999-39";
            scope.config = {
               minView: 'month',
               startView: 'month'
            }
         }

         scope.onTimeSet = function (newDate, oldDate) {
            scope.onChange({time:newDate});
            scope.isOpen = false;
         }

         scope.isOpen = false;
      }
   };
})
.directive('clearDefaultOnFocus', function () {
   return {
      link: function (scope, element) {
         element.on('focus', function (e) {
            if (element.text() === element.attr('default')) {
               element.text('');
            }
         });
         element.on('blur', function (e) {
            if (element.text() === '') {
               element.text(element.attr('default'));
            }
         });
      }
   };
})
.directive('focusIf', function () {
   return {
      restrict: 'A',
      link: function (scope, element, attrs) {
         scope.$watch(function () {
            return scope.$eval(attrs.focusIf);
         }, function (newValue) {
            if (newValue === true) {
               element[0].focus();
            }
         });
      }
   };
})
.directive('autofocus', ['$timeout', function ($timeout) {
   return {
      restrict: 'A',
      link: function ($scope, $element) {
         $timeout(function () {
            $element[0].focus();
         });
      }
   };
}])
.directive('highlightOnFocus', function () {
   return {
      link: function (scope, element) {
         // When user focuses on div, it highlights all the content
         element.on('focus', function (e) {
            var _this = this;
            requestAnimationFrame(function () {
               var range = document.createRange();
               range.selectNodeContents(_this);
               var sel = window.getSelection();
               sel.removeAllRanges();
               sel.addRange(range);
            });
         });
      }
   };
})
.directive('contenteditable', function ($sce) {
   return {
      require: 'ngModel',
      scope: {
         ngModel: '=',
         default: '@'
      },
      link: function (scope, element, attrs, ngModel) {
         element.css({
            minWidth: '15px'
         });

         function read() {
            if (element.text() != element.attr('default')) {
               ngModel.$setViewValue(element.text());
            }
         }

         ngModel.$render = function () {
            element.text(scope.ngModel || scope.default);
         };

         element.on("blur keyup change", function () {
            scope.$apply(read);
         });

         element.on("dragover drop", function (e) {
            e.preventDefault();
            return false;
         });
      }
   };
})
.directive('imageHelper', function () {
   var hasFileReader = typeof FileReader != 'undefined';
   var hasDnd = 'draggable' in document.createElement('span');
   var acceptedTypes = {
      'image/png': true,
      'image/jpeg': true,
      'image/gif': true
   };

   var buttonText = hasDnd ? '<p>drag \'n drop image <br />or click to add</p>' :
      '<p>click to add</p>';
   var imageContainerId = 'dnd-and-view-area';
   var imageContainerCssId = '#' + imageContainerId;
   var fileInputId = 'native-upload-button';
   var fileInputCssId = '#' + fileInputId;
   var innerHTML =
      `
   <div id="${imageContainerId}"
       class="dnd-area"
       style="height: 100%; width: 100%; ">
      <label class="btn"
          style="padding: 10px; height: 100%; width: 100%; display: inline-block;">
         <img  ng-src="{{ fileSource || '/img/tcBrowseImgdnd.png'}}"
             class="pre-dnd" />
         <input id="${fileInputId}"
             type="file"
             style="display: none"
             accept="image/"
             onchange="angular.element(this).scope().onFileSet(this.files)">
      </label>
   </div>`;

   return {
      require: 'ngModel',
      scope: {
         ngModel: '='
      },
      template: innerHTML,
      link: function (scope, element, attrs, ngModel) {
         var imageHolder = element.find(imageContainerCssId);
         var fileInputField = element.find(fileInputCssId);
         var reader = new FileReader();

         function parseFiles(files) {
            var fileData = [];
            scope.fileSources = [];

            for (var i = 0; i < files.length; i++) {
               var file = files[i];

               reader.onload = function (e) {
                  var urlData = e.target.result;
                  scope.fileSource = urlData;
                  scope.ngModel = urlData.split(',')[1];

                  scope.$apply();
               };

               reader.readAsDataURL(file);
            }
         }

         scope.onFileSet = parseFiles;

         if (hasDnd) {
            element.on('dragover', function (e) {
               e.preventDefault();
               e.stopPropagation();
            });
            element.on('dragenter', function (e) {
               e.preventDefault();
               e.stopPropagation();
            });
            element.on('dragleave', function (e) {
               e.preventDefault();
               e.stopPropagation();
            });
            element.on('drop', function (e) {
               e.preventDefault();
               e.stopPropagation();
               parseFiles(e.dataTransfer.files);
            });
         }
      }
   };
});

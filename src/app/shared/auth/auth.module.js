angular
.module('tcAuth', [])
.factory('AuthInterceptor', function ($q, $localStorage) {
   var AuthInterceptor = {
      request: function (config) {
         config.headers = config.headers || {};
         if (!config.external && $localStorage.token) {
            config.headers.Authorization = 'Bearer ' + $localStorage.token;
         }
         return config;
      },
      response: function (response) {
         if (response.status === 401) {
            // handle the case where the user is not authenticated
         }
         return response || $q.when(response);
      }
   }
   return AuthInterceptor;
})
.factory('AuthService', function (UserFactory, ArtistAPI, $localStorage, $q, $http,
   $analytics, $timeout, TunecakesConfig, MessageService) {
   var initialDeferred = $q.defer();

   var _managedArtists = [];
   var _loggedIn = false;
   var _currentUser = null;

   var _setManagedArtists = function() {
      var artistIds = _currentUser ?
         _.union(_currentUser.admin_artists, _currentUser.owned_artists) : [];

      _managedArtists = [];

      var _deferred = $q.resolve();

      /******* TEMPORARY HACK ******/
      if (_currentUser && _currentUser.email === "dork@tunecakes.com") return;
      /******* TEMPORARY HACK ******/

      artistIds.forEach(aid => {
          _deferred = _deferred.then(() => {
            return ArtistAPI.findById(aid).then(artist => {
               _managedArtists.push(artist);
               return $timeout(250);
            });
         });
      });
   }

   var _setVars = function (user, loggedIn, deferred, res) {
      _loggedIn = loggedIn;
      _currentUser = user;
      // TODO: should we check for statuses instead?
      if (res && res.data && res.data.errors) return deferred.reject(res);
      if (user) {
         $analytics.setUserProperties({
            "$user_id": user.id,
            "$first_name": user.first_name,
            "$last_name": user.last_name,
            "$email": user.email,
            "$created": user.created,
            "is_artist": user.is_artist
         });
      }
      _setManagedArtists()
      deferred.resolve(res);
   };

   var _clearLoggedInData = function (deferred, res) {
      delete $localStorage.userId;
      delete $localStorage.token;
      _setVars(null, false, deferred, res);
   };

   var _updateUser = function (deferred) {
      UserFactory.get({
         id: $localStorage.userId
      }).$promise.then(function (user) {
         _setVars(user, true, deferred);
      }).catch((error) => {
         console.log("Login error", error)

         if (error.status >= 500) {
            MessageService.error("Failed to login. Please try again or let us know if this keeps happening");
         } else if (error.status >= 400) {
            _clearLoggedInData(deferred, error);
         } else {
            MessageService.error("Unknown login error occurred. Try refreshing the page. Contact us if this problem continues.");
         }
      });
   };

   // Check if user is logged in already when we first load
   if ($localStorage.userId) {
      _updateUser(initialDeferred);
   } else {
      _clearLoggedInData(initialDeferred);
   }

   return {
      // optional userResource used for logging in after registration
      login: function (user, userResource) {
         var loginDeferred = $q.defer();
         $http
            .post(TunecakesConfig.apiAddress + '/api/authenticate', user)
            .then(function (res) {
               // on success
               $localStorage.userId = res.data.userId;
               $localStorage.token = res.data.token;

               $analytics.setUsername(res.data.userId);

               $analytics.eventTrack("Login");

               if (userResource) {
                  _setVars(userResource, true, loginDeferred);
               } else {
                  _updateUser(loginDeferred);
               }
            }, function (res) {
               // on failure
               _clearLoggedInData(loginDeferred, res);
            });

         return loginDeferred.promise;
      },
      logout: function () {
         _clearLoggedInData(initialDeferred);
      },
      getUser: function () {
         return _currentUser;
      },
      getManagedArtists: function () {
         return _managedArtists;
      },
      isLoggedIn: function () {
         return _loggedIn;
      },
      updateUser: function () {
         var updateDeferred = $q.defer();
         _updateUser(updateDeferred);
         return updateDeferred.promise;
      },
      resolve: function () {
         return initialDeferred.promise;
      },
      isOwner: function (doc) {
         var user = this.getUser();

         if (!this.isLoggedIn()) return false;
         if (user.email === "dork@tunecakes.com") return true;
         if (!doc.owner) return false;
         // Unpopulated Owner
         if (doc.owner === user.id) return true;
         // Populated Owner
         if (doc.owner.id === user.id) return true;

         return false;
      },
      isAdmin: function (doc) {
         var user = this.getUser();

         if (!this.isLoggedIn()) return false;
         if (user.email === "dork@tunecakes.com") return true;
         if (doc.admins) {
            for (var i = 0; i < doc.admins.length; i++) {
               // Unpopulated Admin
               if (doc.admins[i] === user.id) return true;
               // Populated Admin
               if (doc.admins[i].id === user.id) return true;
            }
         }

         return false;
      }
   }
});

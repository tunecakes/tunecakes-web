angular
   .module('tcArtistDropdown', [])
   .directive('tcAddArtists', function () {
      return {
         scope: {
            artists: '='
         },

         templateUrl: '/partials/add_artists.html',
         link: function (scope, ele) {
            scope.focusIndex = 0;

            function addArtist() {
               scope.artists.push({
                  name: ''
               });
               scope.focusIndex = scope.artists.length - 1;
            }

            function removeArtist(artist) {
               scope.artists.splice(scope.artists.indexOf(artist), 1);
               scope.focusIndex = scope.artists.length - 1;
            }

            scope.addArtist = addArtist;
            scope.removeArtist = removeArtist;
         }
      };
   })
   .directive('tcDropdownQuery', function (SearchService) {
      return {
         require: 'ngModel',
         restrict: 'A',
         link: function (scope, ele, attrs, ngModelCtrl) {
            var QueryBuilder = SearchService('dropdown' + scope.$index);
            QueryBuilder.setFilter('artist');
            QueryBuilder.setSearchType('search');

            function formatArtist(artist) {
               return angular.isString(artist.name) ? artist.name : artist;
            }

            function parseArtist(artist) {
               return angular.isString(artist) ? {
                  name: artist
               } : artist;
            }

            ngModelCtrl.$formatters.push(formatArtist);

            ngModelCtrl.$parsers.push(parseArtist);

            ngModelCtrl.$render = function () {
               ele.val(formatArtist(ngModelCtrl.$viewValue));
            };

            scope.select = function (artist, $event) {
               var selectedArtist = {
                  id: artist._source.id,
                  name: artist._source.name
               };
               ngModelCtrl.$setViewValue(selectedArtist);
               ngModelCtrl.$render();
            };

            function handleResults(results) {
               scope.results = results.artists;

               scope.isopen[scope.$index] = results.artists.length > 0 ?
                  true : false;
            }

            QueryBuilder.onResults(handleResults);
            QueryBuilder.onError(console.error);

            ngModelCtrl.$viewChangeListeners.push(function () {
               var val = ngModelCtrl.$viewValue;
               if (angular.isString(val)) {
                  QueryBuilder.setSearchText(val);
                  QueryBuilder.executeQuery();
               }
            });
         }
      };
   });

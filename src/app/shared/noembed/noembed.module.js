angular
.module('tcNoembed', [])
.factory('NoembedService', function ($http, $q, $location, $sce, $timeout) {
   let noembed_host = 'https://noembed.com/embed';

   let getEmbedData = function (url, retries) {
      retries = retries || 0;

      return $http.jsonp($sce.trustAsResourceUrl(noembed_host), {params: {url}})
      .catch(err => {
         if(retries > 5) throw err;

         console.log("Failed to get noembed data. Retrying", err)
         retries++;
         return $timeout(1000).then(() => {
            return getEmbedData(url, retries);
         });
      });
   };

   let isUrlValid = (url) => {
      if (!url) return true;
      var _deferred = $q.defer();

      if (url.includes('youtube.com/user/')) {
         _deferred.resolve(true);
         return _deferred.promise;
      }

      getEmbedData(url)
      //.then(undefined, angular.identity)
      .then(response => {
         if (response.data && response.data.html) {
            console.log('Successfully embedded URL', response)
            return _deferred.resolve(true);
         } else {
            console.log('Failed to embed URL', response)
            return _deferred.reject(false);
         }
      }).catch(err => {
         console.log('Failed to embed URL', url, err)
         _deferred.reject(false);
      });
      return _deferred.promise;
   };

   return {
      getEmbedData,
      isUrlValid
   }
})
.directive('noEmbed', function ($compile, NoembedService) {
   return {
      restrict: 'AC',
      scope: {
         url: '='
      },
      link: function (scope, ele) {
         scope.$watch('url', function (url) {
            if (!url) return;

            if (url === "") ele.html('');

            if (url.includes('youtube.com/user/')) {
               var ytUser = url.split('/').pop();
               var html =
                  '<iframe src="http://www.youtube.com/embed/?listType=user_uploads&list=' +
                  ytUser +
                  '" width="480" height="400" frameBorder="0"></iframe>'
               ele.html(html);
               return;
            }

            NoembedService.getEmbedData(url).then(function (response) {
               if (response.data.error) {
                  console.log("Failed to embed url", response);
                  ele.remove();
                  return;
               }

               ele.html(response.data.html);
            });
         })
      }
   }
});

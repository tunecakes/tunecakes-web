angular
   .module('tcValidation', [])
   .directive('match', function () {
      return {
         require: 'ngModel',
         restrict: 'A',
         scope: {
            match: '='
         },
         transclude: true,
         link: function (scope, elem, attrs, ctrl) {
            scope.$watch(function () {
               return (ctrl.$pristine && angular.isUndefined(ctrl.$modelValue)) ||
                  scope.match === ctrl.$modelValue;
            }, function (currentValue) {
               ctrl.$setValidity('match', currentValue);
            });
         }
      };
   })
   .directive('tcValidate', function () {
      function findFormFieldElements(
         ele,
         form_field_elements) {
         var children = ele.children();
         if (children) {
            for (var i = 0; i < children.length; i++) {
               var child = children[i];
               if (child.attributes &&
                  child.attributes.name &&
                  child.attributes['ng-model']) {
                  form_field_elements.push(child);
               }
               findFormFieldElements(angular.element(child), form_field_elements);
            }
         } else {
            return null;
         }
      }

      return {
         restrict: 'AC',
         link: function (scope, ele, attr) {
            var loaded = false;
            var waiting = false;
            var form_field_elements = [];
            var id = attr.tcValidateId;

            // FIXME: now that I pulled in JQUERY this might be easier?
            findFormFieldElements(ele, form_field_elements);

            var to_watch;
            if (typeof scope[attr.tcValidate] == 'function') {
               // Pass either a 'parse function' that returns the object to validate
               to_watch = scope[attr.tcValidate];
            } else {
               // or pass the object itself
               to_watch = attr.tcValidate;
            }
            scope.$watch(to_watch, checkElements, true);

            var form_ctrl = ele.data().$formController;

            var getButton;
            // allow to pass in a button id in case it's not inside the form or you want to be explicit
            if (attr.buttonId) {
               getButton = function () {
                  return $("#" + attr.buttonId);
               };
            } else {
               getButton = function () {
                  return ele.find('button[type="submit"]');
               };
            }

            // Keep button disabled until form is valid and ready
            scope.$watch(function () {
               return waiting ||
                  form_ctrl.$pending ||
                  form_ctrl.$invalid ||
                  form_ctrl.$pristine;
            }, function (new_val, old_val) {
               var form_button = getButton();
               if (new_val) {
                  // form_button.attr('disabled', '');
               } else {
                  form_button.removeAttr('disabled');
               }
            });

            function checkElements(finished_model) {
               function validator(res) {
                  _.each(form_field_elements, function (form_field_element) {
                     var is_valid = true;
                     var name = form_field_element.getAttribute('name');

                     // res on error is different than on success, check fields on error
                     if (res && res.data) {
                        for (var error_field in res.data.errors) {
                           if (name == error_field) {
                              is_valid = false;
                           }
                        }
                     }

                     angular
                        .element(form_field_element)
                        .data()
                        .$ngModelController
                        .$setValidity('serverSide', is_valid);

                     waiting = false;
                  });
               }

               // don't call when the page first loads
               if (loaded) {
                  waiting = true;
                  scope[attr.tcResource].validate(
                     finished_model,
                     validator, // needs to run on success to clear out previous errors
                     validator
                  );
               } else {
                  loaded = true;
               }
            }
         }
      };
   });

angular.module('tcNotifications', [])
   .config(function ($stateProvider) {
      $stateProvider
         .state('parent.notifications', {
            url: '/notifications',
            templateUrl: '/partials/notifications.html',
            controller: 'notificationsCtrl',
            access: {
               requiredLogin: true
            }
         });
   })
   .controller('notificationsCtrl', function ($scope, NotificationService) {
      $scope.NotificationService = NotificationService;
   })
   .factory('NotificationService',
      function (AuthService, NotificationFactory, $interval, $q, $http, TunecakesConfig) {
         var notifications = [];
         var unread = 0;
         var unseen = 0;

         function updateNotifications(opts) {
            if (AuthService.isLoggedIn()) {

               var params = _.extend({
                  id: AuthService.getUser().id
               }, opts);

               // FIXME: just a workaround for now, if people had retrieved more notifs
               // then retrieve the same amount when updating
               if (opts && !opts.limit && notifications.length > 0)
                  params.limit = notifications.length;

               NotificationFactory.get(params).$promise.then(function (nots) {
                  unread = nots.unread;
                  unseen = nots.unseen;

                  addNotifications(nots.results);
                  cleanNotifications(opts);
               });
            }
         }

         function addNotifications(nots) {
            let p = $q.resolve()

            // Maintain order of notifications
            nots.forEach(n => {
               notifications.push(n)
               //let req = $http.post(TunecakesConfig.apiAddress + '/api/' + n.actor_type + '/' + n.actor + '/transform_feed_item', n)
               //p = p.finally(() => {
               //   return req.then(res => res.data));
               //});
            })
         }

         function fetchMore(lim) {
            var opts = {};
            if (lim) opts.limit = lim;
            opts.id_lt = _.last(notifications).id;

            var params = _.extend({
               id: AuthService.getUser().id
            }, opts);

            NotificationFactory.get(params).$promise.then(function (nots) {
               notifications = notifications.concat(nots.results);
               cleanNotifications(opts);
            });
         }

         // Clean up is_read, see http://stackoverflow.com/questions/35067268
         function cleanNotifications(opts) {
            if (opts && opts.mark_read) {
               opts.mark_read.forEach(function (id) {
                  _.findWhere(notifications, {
                     id: id
                  }).is_read = true;
               });
            }
         }

         function markRead(notification) {
            if (notification.id) {
               cleanNotifications({
                  mark_read: [notification.id]
               });
            }
         }

         /* TODO: there's a possibility of a race condition here,
          where a notif gets marked as seen before it's actually seen.
          We may want to try http://getstream.io/docs/#realtime */
         function markSeen() {
            unseen = 0;
            updateNotifications({
               mark_seen: true
            });
            
            cleanNotifications({
               mark_seen: true
            });
         }

         // Update every 5 minutes
         $interval(updateNotifications, 300 * 1000)
         updateNotifications();

         return {
            getNotifications: function () {
               return notifications;
            },
            getUnread: function () {
               return unread;
            },
            getUnseen: function () {
               return unseen;
            },
            fetchMore: fetchMore,
            updateNotifications: updateNotifications,
            markRead: markRead,
            markSeen: markSeen
         }
      })
   .factory('NotificationFactory', function (FeedFactory) {
      return FeedFactory('user', 'notifications');
   })
   .directive('tcNotifiCard', function (NotificationService, TunecakesConfig, $http) {
      return {
         priority: 1001,
         templateUrl: '/partials/notification_card.html',
         scope: {
            data: '&'
         },
         link: function (scope) {
            let item = scope.data();
            $http.post(TunecakesConfig.apiAddress + '/api/' + item.actor_type + '/' + item.actor + '/transform_feed_item', item)
            .then(res => scope.feedItem = res.data)
            .catch(err => {
               console.log('Unable to convert notification', item, err)
            });

            scope.NotificationService = NotificationService;
         }
      };
   });

angular
   .module('tcMessages', ['toastr'])
   .config(function (toastrConfig) {
      angular.extend(toastrConfig, {
         positionClass: 'toast-top-center',
         target: '#site-container',
         closeButton: true
      });
   })
   .factory('MessageService', function (toastr) {
      function addError(msg) {
         toastr.error(msg);
      }

      function addSuccess(msg) {
         toastr.success(msg);
      }

      function parseHttpError(res) {
         var errors = res.data.errors;
         for (var i = 0; i < errors.length; i++) {
            addError(errors[i]);
         }
      }

      function successAfterRedirect(promise, msg) {
         promise.then(function () {
            addSuccess(msg);
         });
      }

      function errorAfterRedirect(promise, msg) {
         promise.then(function () {
            addError(msg);
         })
      }

      function parseHttpErrorAfterRedirect(promise, msg) {
         promise.then(function () {
            parseHttpError(msg);
         })
      }

      return {
         error: addError,
         success: addSuccess,
         parseHttpError: parseHttpError,
         successAfterRedirect: successAfterRedirect,
         errorAfterRedirect: errorAfterRedirect,
         parseHttpErrorAfterRedirect: parseHttpErrorAfterRedirect
      };
   });

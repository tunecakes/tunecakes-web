/**
 * Created by difernan on 5/4/15.
 */

var util = (function () {
   return {
      validators: {
         email: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/,
         zip: /[0-9]{5}/
      }
   };
})();
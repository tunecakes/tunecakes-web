/**
 * TUNECAKES WEB CLIENT
 */
// require('newrelic');
var express = require('express'),
   proxy = require('express-http-proxy'),
   path = require('path'),
   app = express(),
   config = require('config'),
   favicon = require('serve-favicon'),
   url = require('url');

app.use(favicon(config.distribution_directory + '/img/favicon.ico'));

// pulled from http://stackoverflow.com/a/31144924/1815486
function requireHTTPS(req, res, next) {
   // The 'x-forwarded-proto' check is for Heroku
   if (!req.secure && req.get('x-forwarded-proto') !== 'https') {
      return res.redirect('https://' + req.get('host') + req.url);
   }
   next();
}

if (config.util.getEnv('NODE_ENV').startsWith('prod')) {
   app.use(requireHTTPS);
}

if (config.get('api.proxy')) {
   app.use('/api', proxy(config.get('api.address'), {
      forwardPath: function (req, res) {
         return '/api' + url.parse(req.url).path;
      }
   }));
}

app.use(express.static(config.distribution_directory));

/**
 * Serve up every other path to the angular app, which is in index.html.
 */
app.get('*', function (req, res) {
   res.sendFile(path.join(__dirname, '../dist/index.html'));
});

app.listen(config.web.port, config.web.host);
console.log('listening on port ' + config.web.port + " on host " + config.web.host);

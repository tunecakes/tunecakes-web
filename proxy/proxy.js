var express = require('express');
var proxy = require('http-proxy-middleware');

var app = express();

app.use('/api', proxy({
   target: 'https://api.tunecakes.com',
   changeOrigin: true,
   onProxyRes: function (proxyRes, req, res) {
      proxyRes.headers['Access-Control-Allow-Origin'] = "*"
   }
}));

app.listen(3001);

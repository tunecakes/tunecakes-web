WEB_TAG=tunecakes/web:0.0.1
HEROKU_TAG=registry.heroku.com/tunecakes-web-production/web
WEB_NAME=tunecakes-web
TC_NETWORK=tunecakes-net
WEB_PORT=3000
LIVE_RELOAD_PORT=35729
WEB_BASE_DIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))
WEB_SOURCE_DIR=${WEB_BASE_DIR}src
WEB_CONFIG_DIR=${WEB_BASE_DIR}config
WEB_GRUNTFILE=${WEB_BASE_DIR}Gruntfile.js
WEB_DIST_DIR=${WEB_BASE_DIR}dist
IMAGE_DIR=image


.PHONY: clear-cache
clear-cache:
	aws cloudfront create-invalidation --distribution-id E2MX45T8NS1R2M --paths "/*"

.PHONY: s3-upload
s3-upload:
	rm -rf ./dist
	docker run --rm ${WEB_TAG} ls -al /src/app/dist
	docker rm -f ${WEB_NAME} || true
	docker create --name ${WEB_NAME} ${WEB_TAG}
	docker cp ${WEB_NAME}:/src/app/dist ./
	ls -al ./
	aws s3 sync ./dist/ s3://prod-tunecakes-web/

.PHONY: build-api-proxy
build-api-proxy:
	cd proxy && docker build -t tunecakes-api-proxy .

.PHONY: api-proxy
api-proxy:
	docker rm -f tunecakes-api-proxy || true
	docker run \
		-p 3001:3001 \
		--name tunecakes-api-proxy \
		-d tunecakes-api-proxy

.PHONY: build
build: rm-images
	sudo rm -rf ./dist || true
	docker build -t ${WEB_TAG} ./
	docker run --rm ${WEB_TAG} ls -al /src/app/dist

.PHONY: save
save:
	mkdir -p ${IMAGE_DIR}
	docker save ${WEB_TAG} > ${IMAGE_DIR}/app.tar

.PHONY: load
load:
	docker load -i ${IMAGE_DIR}/app.tar

build-heroku-production:
	rm -rf dist
	rm -rf node_modules
	docker build \
		-e NODE_ENV=production \
		-e NODE_APP_INSTANCE=heroku \
		-t ${HEROKU_TAG} ./

deploy-heroku-production:
	docker push ${HEROKU_TAG}

.PHONY: logs
logs:
	docker logs -f ${WEB_NAME}

.PHONY: rm-images
rm-images:
ifeq ($(shell docker images -q ${WEB_TAG} | wc -l),0)
	@echo "${WEB_TAG} images don't exist. Skipping rmi..."
else
	docker images -q ${WEB_TAG} | xargs docker rmi -f
endif

.PHONY: rm
rm:
ifeq ($(shell docker ps --filter="name=${WEB_NAME}" -a | wc -l),2)
	docker rm -f ${WEB_NAME}
else
	@echo "${WEB_NAME} doesn't exist. Skipping rm..."
endif

.PHONY: run
run: rm
	#npm install
	docker run --name ${WEB_NAME} -d \
		-e HOST=0.0.0.0 \
		-e "TERM=xterm-256color" \
		-e NODE_ENV=local \
		-e FACEBOOK_APP_ID=608947692502876 \
		--network ${TC_NETWORK} \
		-p ${WEB_PORT}:${WEB_PORT} \
		-p ${LIVE_RELOAD_PORT}:${LIVE_RELOAD_PORT} \
		-v ${WEB_SOURCE_DIR}:/src/app/src \
		-v ${WEB_CONFIG_DIR}:/src/app/config \
		-v ${WEB_GRUNTFILE}:/src/app/Gruntfile.js \
		${WEB_TAG} \
		/bin/sh -c "npm start"

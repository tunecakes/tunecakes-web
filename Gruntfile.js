var config = require('config');

config.env = config.util.getEnv('NODE_ENV');

module.exports = function (grunt) {
   var build_tasks = [
      'clean:all',
      'jshint',
      'copy',
      'ejs',
      'sass',
      'concat',
      'babel',
      'clean:tmp'
   ];

   var run_tasks = build_tasks.concat(['express'])

   grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      clean: {
         options: {
            force: true
         },
         all: ["dist", "tmp"],
         tmp: ["tmp"]
      },
      ejs: {
         all: {
            cwd: 'src/app/config',
            src: ['mixpanel.config.ejs', 'app.config.ejs'],
            dest: config.config_directory,
            options: {config},
            ext: '.config.js',
            expand: true
         },
         index: {
            cwd: 'src/app',
            src: ['index.ejs'],
            dest: config.distribution_directory,
            options: {config},
            expand: true,
            ext: '.html'
         },
      },
      copy: {
         assets: {
            expand: true,
            cwd: 'src/assets',
            src: ['**'],
            dest: config.distribution_directory
         },
         views: {
            expand: true,
            cwd: 'src/app',
            src: ['**/*.html'],
            dest: config.partials_directory,
            flatten: true
         },
         tmp: {
            expand: true,
            flatten: true,
            cwd: 'src/app',
            src: ['**/*.js'],
            dest: config.tmp_directory
         }
      },
      jshint: {
         all: [
            'Gruntfile.js',
            'src/app/components/**/*.js',
            'src/app/shared/**/*.js'
         ],
         options: {
            force: true,
            jshintrc: '.jshintrc'
         }
      },
      concat: {
         dist: {
            options: {
               banner: '(function(angular){\n',
               footer: '\n})(window.angular);'
            },
            src: [config.tmp_directory + '/*.module.js', config.tmp_directory +
               '/*.js'
            ],
            dest: config.tmp_directory + '/app_concat.js'
         }
      },
      rev: {
         files: {
            src: ['dist/**/*.{js,css}', '!dist/js/shims/**']
         }
      },

      useminPrepare: {
         html: config.tmp_directory + '/index.html'
      },

      usemin: {
         html: ['dist/index.html']
      },

      sass: {
         options: {
            sourceMap: true
         },
         dist: {
            files: {
               './dist/css/main.css': './src/styles/main.scss'
            }
         }
      },

      uglify: {
         options: {
            report: 'min',
            mangle: true
         }
      },
      ngAnnotate: {
         options: {
            singleQuotes: true
         },
         app: {
            files: [{
               expand: true,
               cwd: 'app/js',
               src: '*.js',
               dest: config.tmp_directory + '/js', // Destination path prefix
               ext: '.js', // Dest filepaths will have this extension.
               extDot: 'last' // Extensions in filenames begin after the last dot
            }]
         }
      },
      express: {
         dev: {
            options: {
               script: 'src/server.js',
               background: true
            }
         }
      },
      watch: {
         scripts: {
            files: ['src/**/*.js', 'src/**/*.ejs', 'src/**/*.html', 'src/**/*.scss',
               'src/**/*.css', '!src/assets/libs/**/*', '!src/**/.*', 'Gruntfile.js'
            ],
            tasks: run_tasks,
            options: {
               spawn: false,
               livereload: true
            }
         },
         grunt: { files: ['Gruntfile.js']  }
      },
      babel: {
         options: {
            sourceMap: true,
            presets: ['es2015']
         },
         dist: {
            files: {
               'dist/app/app.js': 'tmp/app_concat.js'
            }
         }
      }
   });

   grunt.loadNpmTasks('grunt-contrib-clean');
   grunt.loadNpmTasks('grunt-contrib-copy');
   grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-contrib-watch');
   grunt.loadNpmTasks('grunt-express-server');
   grunt.loadNpmTasks('grunt-babel');
   grunt.loadNpmTasks('grunt-ejs');
   grunt.loadNpmTasks('grunt-sass');
   grunt.loadNpmTasks('grunt-contrib-jshint');

   // Tell Grunt what to do when we type "grunt" into the terminal
   grunt.registerTask('default', run_tasks.concat(['watch']));

   grunt.registerTask('build', build_tasks);
};

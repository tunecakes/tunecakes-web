FROM alpine:3.6

ENV NODE_ENV production 

WORKDIR /src/app
ADD . .
RUN apk add --no-cache --update nodejs nodejs-npm git \
   && npm install \
   && npm install -g grunt-cli bower

RUN bower install --allow-root

# Install node_modules in root dir so we can use them
# after volume mount in dev environment
RUN npm run compile

CMD ["node", "src/server.js"]

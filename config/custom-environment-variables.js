module.exports = {
   web: {
      host: "HOST",
      port: "PORT"
   },
   api: {
      address: "TUNECAKES_API_ADDRESS"
   },
   mixpanel_api_key: "MIXPANEL_API_KEY",
   facebook_app_id: "FACEBOOK_APP_ID"
};

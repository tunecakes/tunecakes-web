var path = require('path');
var rootPath = path.normalize(__dirname + '/..');
var dataDir = rootPath;
var distDir = dataDir + '/dist';
var viewsDir = distDir + '/views';
var partialsDir = distDir + '/partials';

module.exports = {
   app: {
      name: 'TuneCakes'
   },
   env: 'production',
   session_key: 'whatchamacallit',
   api: {
      proxy: true,
      address: 'http://localhost:3001',
      host: 'localhost',
      port: 3001
   },
   web: {
      host: 'localhost',
      port: 3000
   },

   google_api_key: 'AIzaSyB9VHjz2EHhR2_M7vMBcQkdcnRhpgwj4Ig',
   facebook_app_id: '263421307395016',

   debug: false,
   root: rootPath,
   config_directory: distDir + '/config',
   distribution_directory: distDir,
   views_directory: viewsDir,
   partials_directory: partialsDir,
   tmp_directory: dataDir + '/tmp',

   index_path: viewsDir + '/index.html',
   app_path: distDir + '/app/app.js',

   mixpanel_api_key: "9d39fa505394a274925af3db83003d17"
};
